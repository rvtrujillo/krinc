
#include "320sh.h"

char* prev_path;
char* current_path;

int ret_status;

int linec, currentLineIndex, printLineIndex = 0;
Line** lines;
char* current_line;
int numPipes;


char*** pipe_child_args(char ** shell_args, int argc) {
	int i, j;
	int arg_list_count = 0;
	int lastPipeIndex = 0;
	char ***ptr = calloc(sizeof(char**), numPipes+1);
	for(i=0; i< argc; i++){
		if(!(strcmp(shell_args[i], "|"))) {
			if(lastPipeIndex==0){
				ptr[arg_list_count] = calloc(sizeof(char*), i+1);
				for(j=0; j<i; j++){
					ptr[arg_list_count][j] = calloc(strlen(shell_args[j])+1,1);
					strcpy(ptr[arg_list_count][j],shell_args[j]);
				}
				
				ptr[arg_list_count][j] = NULL;
				arg_list_count++;
			}
			else{
				int z = 0;
				ptr[arg_list_count] = calloc(sizeof(char*), i-lastPipeIndex-1);
				for(j=lastPipeIndex+1; j<i; j++){
					ptr[arg_list_count][z] = calloc(strlen(shell_args[j])+1,1);
					strcpy(ptr[arg_list_count][z],shell_args[j]);
					z++;
				}
				ptr[arg_list_count][z] = NULL;
				arg_list_count++;
			}
			lastPipeIndex = i;
		}

	}
	i=0;
	ptr[arg_list_count] = calloc(sizeof(char*), argc-lastPipeIndex);
	for(++lastPipeIndex;lastPipeIndex<argc;lastPipeIndex++){
		ptr[arg_list_count][i] = calloc(strlen(shell_args[lastPipeIndex])+1,1);
		strcpy(ptr[arg_list_count][i],shell_args[lastPipeIndex]);
		i++;
	}
	ptr[arg_list_count][i] = NULL;

	return ptr;
 }

 void create_pipe(char** shell_args, int argc, char** envp) {
 	//const int commands = countPipes + 1;
	if(numPipes==0)
		return;
	
	int i;
	char*** child_args = pipe_child_args(shell_args, argc);
	int fd[numPipes * 2];
	int first = 0;

 	 //CHECK IF THERE ARE CORRECT AMOUNT OF COMMANDS & INIT PIPES 
	for(i = 0; i < numPipes; i++){
		if(pipe(fd + i*2) < 0) {
			fprintf(stderr, "%s\n", "Wrong number of commands!");
			exit(EXIT_FAILURE);
		}
	}

	pid_t c_pid;
	int status;
	for(i = 1; i < numPipes + 2; i++){
		
		if(first == 0) {
			//pipe(fd);
			if((c_pid = Fork()) == 0) {
				close(fd[0]);
				first++;
				dup2(fd[1], STDOUT_FILENO);
				execvpe(child_args[0][0], child_args[0], envp);
				
				fprintf(stderr, "%s\n\n\n\n\n\n\n", "THIS IS STILL HERE!!!!!");
				exit(0);
				
			} 
			else {
				close(fd[1]);
				wait(&status);
				first++;
			}
		}
		else if(first == 1){
			//pipe(fd);
			if((c_pid = Fork()) == 0){
				close(fd[(i-1)*2]);
				dup2(fd[((i-1)*2)+1], STDOUT_FILENO);
				dup2(fd[((i-1)*2)-2], STDIN_FILENO);
				first++;
				execvpe(child_args[i - 1][0], child_args[i - 1], envp);
				exit(0);
			}
			else{
				close(fd[((i-1)*2)+1]);
				close(fd[((i-1)*2)-2]);
				wait(&status);
				first++;
			}
		}
	}
}

int main (int argc, char ** argv, char **envp) {

 	int finished = 0;
 	ret_status = 0;
 	int z = 0;
 	int countPtr;
 	char *prompt = "320sh> ";
 	char cmd[MAX_INPUT] = {'0'};
 	int shell_argc = 0;
 	pid_t pid = 0;
 	int opt, debug, child_status;
 	prev_path = getDir();
 	current_path = getDir();
 	char* w_dir = NULL;

	/* CREATE PATH TO COMMAND LINE HISTORY FILE IN USER'S HOME DIRECTORY */
 	char* history_path = calloc(strlen(getenv("HOME"))+20,1);
 	strcat(history_path, getenv("HOME"));
 	strcat(history_path,"/.320sh_history.txt");

	/* CREATE FD TO COMMAND LINE HISTORY FILE */
 	FILE* history_fd = get_fd(history_path,"r");
	/* ALLOCATE MEMORY FOR COMMAND LINE HISTORY */
 	allocate_history(&lines);

	/* CHECK IF THERE WAS ANY HISTORY */
 	if(history_fd==NULL)
 		currentLineIndex = printLineIndex = linec = 0;
 	else{
 		linec = copy_history(history_fd, lines);
 		fclose(history_fd);
 		currentLineIndex = printLineIndex = linec;
 	}

	/* CHECK IF IN DEBUGGING MODE */
 	if ((opt = getopt(argc, argv, "d")) != -1){
 		debug = 1;
 	}
 	else debug = 0;

 	while (!finished) {
 		errno = 0;
 		char *cursor;
 		char last_char;
 		int rv;
 		int count;
 		char * ptr;
 		
		/* PRINT THE PROMPT WITH CWD */
 		w_dir = (char*)calloc(strlen(getDir()), 1); 
 		w_dir = getDir();
 		prompt = (char*)calloc(strlen(w_dir)+14, 1);
 		strcat(prompt,"[");
 		strcat(prompt,w_dir);
 		free(w_dir);
 		strcat(prompt,"] 320sh> ");

 		rv = write(1, prompt, strlen(prompt));
 		if (!rv) { 
 			finished = 1;
 			break;
 		}

    	/* READ AND PARSE THE INPUT */
    	int arrow = 0;
    	int numLeft = 0;

    	/* KEEP TRACK OF NUMBER OF QUOTES & PIPES */
    	int numQuotes=0;
    	numPipes = 0;

 		for(rv = 1, count = 0, cursor = cmd, last_char = 1; rv && (++count < (MAX_INPUT-1))	&& (last_char != '\n'); cursor++) { 

 			rv = read(0, cursor, 1);
 			last_char = *cursor;
			int line_len;

 			/* DETECT CTRL+C */
 			if(last_char == 3) {
 				/* WRITE FUTURE PROMPT ON NEW LINE */
 				write(1, "\n", 2);
 				
 				/* FREE LEFTOVER MEMORY */
 				free(prompt);
 				free(prev_path);
 				free(current_path);

				/* PROGRAM ENDING. SAVE COMMAND LINE HISTORY IN FILE */
 				//save_history(history_path,currentLineIndex-1, lines);
				/* FREE ALL SPACE ALLOCATED FOR COMMAND MEMORY */
 				free_history(lines);

 				/* HANDLE THE CTRL+C SIGNAL - SIGINT */
 				if(signal(SIGINT, sigint_handler) == SIG_ERR)
 					fprintf(stderr, "SIGNAL ERROR\n");

 				/* IF HERE, THEN SIGNAL HANDLER FAILED. END PROGRAM W/ EXIT_FAILURE */
 				return EXIT_FAILURE;

 			} /* DETECT CTRL+A */
 			else if(last_char == 1){
 				cursor--;
				/* CHECK IF AT BEGINNING OF LINE (CAN'T GO PAST PROMPT) */ 
				while(cursor-numLeft+1 != cmd){
					write(1,"\033[D", 3);
					numLeft++;
				}
				count--;
				continue;
 			}
 			else if(last_char == 12){
 				cursor--;
 				count--;
 				//char temp[] = {'c','l','e','a','r','\0','\n'};
 				cursor = cursor - count+1;
 				strcpy(cursor,"clear \n");
 				count = 6;
 				break;

 			}
 			else if(last_char == 127 || last_char == 8){
 				cursor--;
 				count--;
 				if(cursor-numLeft+1 != cmd){
					char* temp = cmd; 
					remove_char(&temp, count-numLeft);
					cursor--;
					count--;
					for(int i = 0; i<numLeft;i++ ){
    					write(1,"\033[C", 3);
    				}
    				write(1,"\b \b", 3);

    				for(int i=0; i<numLeft; i++){
    					write(1,"\b",1);
    					write(1,cursor-i,1);
    					write(1,"\b",1);
    				}
				}
				continue;
 			} /* IF THE FIRST CHAR IS ESC */
			else if (last_char == 27){
				arrow++;
				count--;
				cursor--;
				continue;
			} /* IF THERE WAS THE 2ND BYTE FOR AN ARROW */
    		else if(last_char == '[' && arrow==1){
    			arrow++;
    			count--;
				cursor--;
				continue;
    		}
       		else if(arrow==2){
       			switch(last_char) { /* THE IDENTIFIER FOR WHICH ARROW KEY WAS PRESSED */
    				case 'A':
            			/* CODE FOR ARROW UP */
        				/* CHECK IF THERE IS A PREV LINE. IF NOT DO NOTHING. ELSE, MODIFY PRINTLINEINDEX */
	    				if(linec<1 || ((lines[printLineIndex]->prev_line->num == currentLineIndex) && linec==500) || (linec<500 && printLineIndex==0)){
	    					/* SET VARIABLES BACK TO NORMAL VALUES */
        					cursor--;
        					count--;
	    					arrow=0;
	    					continue;
	    				}

	    				/* INCREMENT PRINTLINEINDEX TO END IF AT ZERO */
	    				if(linec==500 && printLineIndex==0 && currentLineIndex!=499){
	    					printLineIndex=499;
	    				}
	    				else printLineIndex--;
	        			
	        			/* DELETE EVERYTHING ON THE LINE */
	        			line_len = count + strlen(prompt);
	        			cursor--;
        				count--;
	        			while(numLeft != 0){
        					write(1,"\033[C", 3);
        					numLeft--;
        				}
	        			for(int d = 0; d < line_len; d++){
	        				write(1, "\b \b", 3);
	        			}

	        			/* WRITE THE PREV LINE */
	        			write(1, prompt, strlen(prompt));
	        			write(1, lines[printLineIndex]->str, strlen(lines[printLineIndex]->str));

	        			/* SET VARIABLES BACK TO NORMAL */
	        			/* CLEAR CMD FOR THE NEW LINE */
				    	ptr = cmd;
				    	for(countPtr = 0; countPtr < count - 1; countPtr++) {
				    		ptr[countPtr] = '\0';
				    	}
				    	count = strlen(lines[printLineIndex]->str);
				    	strcpy(cmd,lines[printLineIndex]->str);
				    	cursor = cmd + count - 1;

	        			arrow=0;
    					continue;
    				case 'B':
        			    /* CODE FOR ARROW DOWN */
        				/* CHECK IF THERE IS A NEXT LINE. IF NOT DO NOTHING */
	    				if(linec<2 || (printLineIndex == currentLineIndex)){
	    					/* SET VARIABLES BACK TO NORMAL VALUES */
	    					arrow=0;
	    					cursor--;
        					count--;
	    					continue;
	    				}
	    				printLineIndex = lines[printLineIndex]->next_line->num;
	        			/* DELETE EVERYTHING ON THE LINE */
	        			line_len = count + strlen(prompt);
	        			cursor--;
        				count--;
        				/* IF CURSOR IS NOT AT END PUT AT END AND THEN DELETE LINE */
	        			while(numLeft != 0){
        					write(1,"\033[C", 3);
        					numLeft--;
        				}
	        			for(int d = 0; d < line_len; d++){
	        				write(1, "\b \b", 3);
	        			}

	        			write(1, prompt, strlen(prompt));
	        			write(1, lines[printLineIndex]->str, strlen(lines[printLineIndex]->str));
	        			
	        			/* CLEAR CMD FOR THE NEW LINE */
				    	ptr = cmd;
				    	for(countPtr = 0; countPtr < count - 1; countPtr++) {
				    		ptr[countPtr] = '\0';
				    	}
				    	count = strlen(lines[printLineIndex]->str);
				    	strcpy(cmd,lines[printLineIndex]->str);
				    	cursor = cmd + count - 1;
	        			arrow=0;
        				continue;
        			case 'C':
            			/* CODE FOR ARROW RIGHT */

        				cursor--;
        				count--;
        				if(numLeft != 0){
        					write(1,"\033[C", 3);
        					numLeft--;
        				}

        				arrow = 0;
           				continue;
        			case 'D':
           				/* CODE FOR ARROW LEFT */
        				cursor--;
        				/* CHECK IF AT BEGINNING OF LINE (CAN'T GO PAST PROMPT) */ 
        				if(cursor-numLeft+1 != cmd){
           					write(1,"\033[D", 3);
        					numLeft++;
        				}
        				arrow=0;
        				count--;
           				continue;
        		}
       		}
        	else {
           		/* ---------- WRITE CHAR DEPENDING ON WHERE THE CURSOR IS ---------- */
        		if(last_char == '"')
        			numQuotes++;
        		if(last_char == '|')
        			numPipes++;
        		arrow = 0;
        		/* CHECK IF WRITING A CHARACTER IN THE MIDDLE SOMEWHERE */
        		if(numLeft != 0 && last_char != '\n'){
        			/* SHIFT EVERYTHING OVER BY 1 */
        			char* temp = cmd;
        			insert_char(&temp, last_char, count-numLeft-1);
        			temp = cursor - numLeft;
        			write(1,temp,numLeft+1);
					for(int i=0;i<numLeft;i++)
						write(1, "\b", 1);
        		 }
        		else 
        			write(1, &last_char, 1);
        		/* ---------- WRITE CHAR DEPENDING ON WHERE THE CURSOR IS ---------- */
        	}
        }
		
		/* BEGIN PARSING SHELL ARGS */
		cursor = cmd;
		/* GET RID OF NEW LINE CHARACTER AT THE END */
		cursor[strlen(cursor)-1] = '\0';
		insert_spaces(&cursor);
		shell_argc = count_sargc(cursor);
		if(shell_argc == 0)
			continue;
		/* INIT AND FILL SHELL_ARGS. +1 FOR NULL TERMINATING ARG AT END */
		char** shell_args = (char**)calloc(shell_argc + 1, sizeof(char*));
		init_sargs(cursor, &shell_args);
		shell_args[shell_argc] = NULL;
		cursor = cmd;

		 //pipe_child_args(shell_args, shell_argc);
		

		/* ------------------ MODIFY COMMAND LINE HISTORY ------------------ */
    	free(lines[currentLineIndex]->str);
    	lines[currentLineIndex]->str = calloc(1, strlen(cmd)+1);
    	strncpy(lines[currentLineIndex]->str, cmd, count - 2);
    	strcat(lines[currentLineIndex]->str, "\0");

    	if(linec < 500){
    		linec++;
    		currentLineIndex++;
    		printLineIndex = currentLineIndex;
    	}
    	else if(linec == 500){
    		if(currentLineIndex==499){
    			currentLineIndex = 0;
    		}
    		else{
    			currentLineIndex++;
    		}
    		printLineIndex = currentLineIndex;
    	}

    	/* CLEAR CMD FOR THE NEXT LINE */
    	ptr = cmd;
    	for(countPtr = 0; countPtr < count - 1; countPtr++) {
    		ptr[countPtr] = '\0';
    	}
    	/* ------------------ MODIFY COMMAND LINE HISTORY ------------------ */

		/* CHECK IF pwd COMMAND WAS ENTERED */
		if(strcmp(shell_args[0],"pwd") == 0){
			if((pid = Fork()) == 0){
				/* IF DEBUG MODE, DISPLAY DEBUGGING INFO */
				if(debug)
					fprintf(stderr, "RUNNING: %s\n", shell_args[0]);
				/* DO ANY REDIRECTIONS IF NECESSARY */
				if(redirect(shell_args, shell_argc))
				{
					ret_status = 1;
					goto exit;
				}
				/* POSSESS THE CHILD PROCESS */
				pwd_cmd();
				goto exit;
			}
			/* HAVE THE PARENT PROCESS WAIT FOR ITS CHILD TO DIE */
			wait(&child_status);
			ret_status = WEXITSTATUS(child_status);
			/* IF IN DEBUG MODE, PRINT EXIT STATUS OF CHILD PROCESS */
			if(debug && WIFEXITED(child_status))
				fprintf(stderr, "ENDED: %s (ret=%d)\n", shell_args[0], WEXITSTATUS(child_status));
			continue;
		} /* CHECK IF help COMMAND WAS ENTERED */
		else if(strcmp(shell_args[0],"help") == 0){
			if((pid = Fork()) == 0){
				/* IF DEBUG MODE, DISPLAY DEBUGGING INFO */
				if(debug)
					fprintf(stderr, "RUNNING: %s\n", shell_args[0]);
				/* DO ANY REDIRECTIONS IF NECESSARY */
				if(redirect(shell_args, shell_argc))
				{
					ret_status = 1;
					goto exit;
				}
				/* IF DEBUG MODE, DISPLAY DEBUGGING INFO */
				if(debug)
					fprintf(stderr, "RUNNING: %s\n", shell_args[0]);
				/* POSSESS THE CHILD PROCESS */
				help_cmd();
				goto exit;
			}
			/* HAVE THE PARENT PROCESS WAIT FOR ITS CHILD TO DIE */
			wait(&child_status);
			ret_status = WEXITSTATUS(child_status);
			/* IF IN DEBUG MODE, PRINT EXIT STATUS OF CHILD PROCESS */
			if(debug && WIFEXITED(child_status))
				fprintf(stderr, "ENDED: %s (ret=%d)\n", shell_args[0], WEXITSTATUS(child_status));
			continue;
			
		} 	
		else if(strcmp(shell_args[0], "exit") == 0) {
		exit:	/* PROGRAM ENDING. SAVE COMMAND LINE HISTORY IN FILE */
			save_history(history_path,currentLineIndex-1, lines);
			/* FREE ALL SPACE ALLOCATED FOR COMMAND MEMORY */
			free_history(lines);
			for(z = 0; z < shell_argc; z++) {
	    	free(shell_args[z]);
	    	}
	    	free(shell_args);
	    	free(prompt);
			free(prev_path);
			free(current_path);
			exit(0);
		} /* CHECK IF cd COMMAND WAS ENTERED */
		else if(strcmp(shell_args[0],"cd") == 0){
		
			/* IF DEBUG MODE, DISPLAY DEBUGGING INFO */
			if(debug)
				fprintf(stderr, "RUNNING: %s\n", shell_args[0]);
			
			cd_cmd(shell_argc, shell_args);
			
			/* IF IN DEBUG MODE, PRINT EXIT STATUS OF CHILD PROCESS */
			if(debug)
				fprintf(stderr, "ENDED: %s (ret=%d)\n", shell_args[0], ret_status);
			continue;
			
		} /* CHECK IF set COMMAND WAS ENTERED */
		else if(strcmp(shell_args[0], "set") == 0) {
			
			/* IF DEBUG MODE, DISPLAY DEBUGGING INFO */
			if(debug)
				fprintf(stderr, "RUNNING: %s\n", shell_args[0]);
			
			if(shell_args[1]!=NULL){
				set_cmd(shell_argc, shell_args);
				ret_status = 0;
			}
			else{
				fprintf(stderr, "Invalid input for set.\n");
				ret_status = 1;
			}

			/* IF IN DEBUG MODE, PRINT EXIT STATUS OF CHILD PROCESS */
			if(debug)
				fprintf(stderr, "ENDED: %s (ret=%d)\n", shell_args[0],ret_status);
			continue;
		} /* CHECK IF echo COMMAND WAS ENTERED */
		else if(strcmp(shell_args[0], "echo") == 0){
			if((pid = Fork()) == 0){
				/* IF DEBUG MODE, DISPLAY DEBUGGING INFO */
				if(debug)
					fprintf(stderr, "RUNNING: %s\n", shell_args[0]);
				/* DO ANY REDIRECTIONS IF NECESSARY */
				if(redirect(shell_args, shell_argc))
				{
					ret_status = 1;
					goto exit;
				}
				/* POSSESS THE CHILD PROCESS */
				char** child_args = get_child_args(shell_args, shell_argc);
				int child_argc = get_child_arc(child_args);
				echo_cmd(child_argc, child_args, ret_status);
				ret_status = 0;
				goto exit;
			}
			/* HAVE THE PARENT PROCESS WAIT FOR ITS CHILD TO DIE */
			wait(&child_status);
			ret_status = WEXITSTATUS(child_status);
			/* IF IN DEBUG MODE, PRINT EXIT STATUS OF CHILD PROCESS */
			if(debug && WIFEXITED(child_status))
				fprintf(stderr, "ENDED: %s (ret=%d)\n", shell_args[0], WEXITSTATUS(child_status));
			continue;
		}
		else if(strcmp(shell_args[0], "history") == 0){
			if((pid = Fork()) == 0){
				/* IF DEBUG MODE, DISPLAY DEBUGGING INFO */
				if(debug)
					fprintf(stderr, "RUNNING: %s\n", shell_args[0]);
				/* DO ANY REDIRECTIONS IF NECESSARY */
				if(redirect(shell_args, shell_argc))
				{
					ret_status = 1;
					goto exit;
				}
				/* POSSESS THE CHILD PROCESS */
				print_history(stdout, currentLineIndex-1, lines);
				ret_status = 0;
				goto exit;
			}
			/* HAVE THE PARENT PROCESS WAIT FOR ITS CHILD TO DIE */
			wait(&child_status);
			ret_status = WEXITSTATUS(child_status);
			/* IF IN DEBUG MODE, PRINT EXIT STATUS OF CHILD PROCESS */
			if(debug && WIFEXITED(child_status))
				fprintf(stderr, "ENDED-: %s (ret=%d)\n", shell_args[0], WEXITSTATUS(child_status));
			continue;
		}
		else if(strcmp(shell_args[0], "clear-history") == 0){
			free_history(lines);
			allocate_history(&lines);
			currentLineIndex = printLineIndex = linec = 0;
			continue;
		}
		else if(numPipes>0){
			create_pipe(shell_args, shell_argc, envp);
			numPipes = 0;
			fprintf(stderr, "\n\n%s\n\n", "");
			continue;
		}
		/* THE FILE EXISTS SO RUN THE FILE */
		if(findFile(shell_args[0])) {
			if((pid = Fork()) == 0){
				/* IF DEBUG MODE, DISPLAY DEBUGGING INFO */
				if(debug)
					fprintf(stderr, "RUNNING-: %s\n", shell_args[0]);
				/* DO ANY REDIRECTIONS IF NECESSARY */
				if(redirect(shell_args, shell_argc))
				{
					ret_status = 1;
					goto exit;
				}
				char** child_args = get_child_args(shell_args, shell_argc);
				
				free_history(lines);
				for(z = 0; z < shell_argc; z++) {
		    	free(shell_args[z]);
		    	}
		    	free(shell_args);
		    	free(prompt);
				free(prev_path);
				free(current_path);
				/* POSSESS THE CHILD PROCESS */
				execvpe(child_args[0], child_args, envp);
			}
			/* HAVE THE PARENT PROCESS WAIT FOR ITS CHILD TO DIE */
			wait(&child_status);
			ret_status = WEXITSTATUS(child_status);
			/* IF IN DEBUG MODE, PRINT EXIT STATUS OF CHILD PROCESS */
			if(debug && WIFEXITED(child_status))
				fprintf(stderr, "ENDED: %s (ret=%d)\n", shell_args[0], WEXITSTATUS(child_status));
		}
		else {
			/* IF DEBUG MODE, DISPLAY DEBUGGING INFO */
			if(debug)
				fprintf(stderr, "RUNNING: %s\n", shell_args[0]);
			ret_status = 1;
			fprintf(stderr, "%s : %s\n", shell_args[0], strerror(errno));
			/* IF IN DEBUG MODE, PRINT EXIT STATUS OF CHILD PROCESS */
			if(debug)
				fprintf(stderr, "ENDED: %s (ret=%d)\n", shell_args[0], ret_status);
		}
	    *cursor = '\0';
	    if (!rv) { 
	    	finished = 1;
	    	break;
	    }
	    for(z = 0; z < shell_argc; z++) {
	    	free(shell_args[z]);
	    }
	    free(shell_args);
    }
	/* FREE VARIABLES AND RETURN */
	free(prompt);
	free(prev_path);
	free(current_path);
	return EXIT_SUCCESS;
}


/*
 * findFile function
 * @param char* fileName - a string that is the file name (no slash)
 * @return int - returns 0 if file doesn't exist. 1 o/w
 * -- this function will take a file name and append it to each
 *    environmental path and check whether the file exists.
 */
 int findFile(char* fileName){

 	struct stat checkPath;
	/* CHECK IF PROVIDED RELATIVE PATH ALREADY */
 	if(fileName[0] == '/' || fileName[0] =='.') {
 		if(stat(fileName, &checkPath) == -1) {
 			return 0;
 		} else {
 			return 1;
 		}
 	}

	/* ONLY GIVEN FILE NAME. STORE STRING OF ALL ENVIRONMENTAL PATHS */
 	char* envPaths = getenv("PATH");
 	char* temp = (char*)malloc(strlen(envPaths)+1);
 	strcpy(temp,envPaths);
	/* COLON IS THE DELIMETER FOR ENVIRONMENTAL PATHS */
 	char delim[2] = ":";
	/* GET FIRST ENVIRONMENTAL PATH TO START */
 	char* token = strtok(temp, delim);
	/* STRING TO STORE EACH COMPLETE PATH */
 	char* pathToCheck;
	/* CHECK IF ANY OF THE PATHS LEAD TO A FILE THAT EXISTS */
 	while(token != NULL){
        /* ALLOCATE MEMORY FOR COMPLETE PATH TO CHECK
		 * +2 FOR NULL TERMINATOR AND FOR THE ADDED SLASH IN FRONT OF FILENAME */
 		pathToCheck = malloc(strlen(token) + strlen(fileName) + 2);
		/* COPY ENV PATH TO pathToCheck */
 		strcpy(pathToCheck, (token));
		/* ADD SLASH TO END OF pathToCheck */
 		strcat(pathToCheck, "/");
    	/* ADD FILENAME TO COMPLETE THE PATH */
 		strcat(pathToCheck, fileName);

    	/* WRITE PATHS FOR TESTING */
    	/* write(1, pathToCheck, 20); */
 		if(stat(pathToCheck, &checkPath) == -1) {
    		/* SET TOKEN TO NEXT PATH. KEEP LOOKING */
 			token = strtok(NULL, delim);
 			free(pathToCheck);
 		} 
 		else {
 			free(pathToCheck);
 			free(temp);
 			return 1;
 		}
 	}
	/* FILE NOT DOES NOT EXIST */
	free(temp);
 	return 0;
 }


 void sigint_handler(int signum){
 	fflush(stdout);
 	exit(0);
 }

/* 
 * getDir function
 * Finds the current directory by calling getcwd() method.
 * @return char* - a char* pointer to the buffer which is current directory.
 */
 char* getDir(){
	/* PREPARE UPPER BOUND SIZE OF ABSOLUTE PATH */
 	size_t b_size = 4096;
	/* ALLOCATE MEMORY TO STORE ABSOLUTE PATH */
 	char* buff = (char*)calloc(b_size,1);
	/* STORE ABSOLUTE PATH AT buff */
 	char* buff2 = calloc(strlen(getcwd(buff, b_size))+1,1);
 	strcpy(buff2,buff);
 	free(buff);
	/* CHECK FOR ERROR*/
 	if(buff==NULL){
 		printf("Oh dear, something went wrong with getcwd()! %s\n", strerror(errno));
 	}

 	return buff2;
 }

/*
 * pwd_cmd function
 * Writes to the screen the absolute path, obtained by getDir(), to the current directory.
 */
 void pwd_cmd(){
	/* WRITE THE ABSOLUTE PATH TO CURRENT DIRECTORY */
 	write(1,getDir(),strlen(getDir()));
 	write(1,"\n",1);
 	free(getDir());
 	ret_status = 0;
 }

/*
 * cd function
 * @params int argc - arg count from shell command line
 *		   char** args - args from shell command line
 * 3 cases - 
 *		If no args after cd, then cwd becomes home directory
 *		If a '-' arg, the cwd will switch with the last cwd
 *		If anything else, cwd will move into that directory if able.
 */
 void cd_cmd(int argc, char** args){
	/* IF NO ARGS THEN SET CURRENT DIR TO HOME DIR */
 	if(argc==1){
		/* STORE current_path IN temp */
 		char* temp = (char*)malloc(strlen(current_path)+1);
 		strcpy(temp,current_path);
		/* STORE temp IN prev_path */
 		free(prev_path);
 		prev_path = (char*)malloc(strlen(temp)+1);
 		strcpy(prev_path,temp);
		/* SET current_path to HOME DIR */
 		free(current_path);
 		current_path = (char*)malloc(strlen(getenv("HOME")+1));
 		strcpy(current_path,getenv("HOME"));
		/* FREE temp AND CHANGE DIR TO HOME DIR */
 		free(temp);
 		if(chdir(getenv("HOME"))<0){
 			fprintf(stderr,"320sh: cd: %s: No such file or directory\n", args[1]);
 			free(temp);
 			ret_status = 1;
 			return;
 		}
	} /* IF '-' THEN SWITCH PREV AND CURRENT DIRECTORIES */
 		else if(args[1][0] == '-'){
			/* STORE prev_path IN temp */
 			char* temp = (char*)malloc(strlen(prev_path)+1);
 			strcpy(temp,prev_path);
			/* STORE current_path IN prev_path */
 			free(prev_path);
 			prev_path = (char*)malloc(strlen(current_path)+1);
 			strcpy(prev_path, current_path);
			/* STORE prev_path (temp) IN current_path */
 			free(current_path);
 			current_path = (char*)malloc(strlen(temp)+1);
 			strcpy(current_path,temp);
			/* FREE temp */
 			free(temp);
 			if(chdir(current_path)<0){
 				fprintf(stderr,"Oh dear cd could not go back to prev directory.\n");
 				ret_status = 1;
 				return;
 			}
 		}
	else{ /* MODIFY DIRECTORY WITH ARG */
		/* STORE current_path IN temp IN CASE MODIFIED PATH DOESN'T EXIST */
 		char* temp = (char*)malloc(strlen(current_path)+1);
 		strcpy(temp,current_path);
		/* MODIFY current_path */
 		current_path = (char*)realloc(current_path, strlen(current_path)+strlen(args[1])+2);
 		strcat(current_path, "/");
 		strcat(current_path, args[1]);
		/* TRY AND CHANGE CURRENT DIRECTORY */
 		if(chdir(current_path) < 0){
			/* DIRECTORY DOES NOT EXIST, RESTORE current_path W/ TEMP */
 			free(current_path);
 			current_path = (char*)malloc(strlen(temp)+1);
 			strcpy(current_path,temp);
			/* FREE TEMP */
 			free(temp);
 			fprintf(stderr,"320sh: cd: %s: No such file or directory\n", args[1]);
 			ret_status = 1;
 			return;
 		}
		else{ /* MODIFIED DIR DOES EXIST AND HAS BEEN SET TO CURRENT DIR */
			/* STORE OLD current_path VALUE INTO prev_path */
 		free(prev_path);
 		prev_path = (char*)malloc(strlen(temp)+1);
 		strcpy(prev_path,temp);
			/* FREE TEMP */
 		free(temp);
 		}
    }
 ret_status = 0;
 }

/*
 * echo function
 * Writes back to the screen input from the user
 */
 void echo_cmd(int arg_count, char** args, int ret){

	/* CREATE VAR FOR NUM OF SHELL ARGS TO ECHO */
 	int echo_argc = arg_count - 1;
	/* CREATE VAR TO STORE LENGTH OF ECHO STRING */
 	int echo_len = 0;
 	int i = 0;
 	int j = 0;
 	int bad_vars = 0;
	/* CREATE PTR TO FUTURE ECHO STRING */
 	char* echo_str = NULL;

	/* INITIALIZE DOUBLE POINTER EXPANDED_ARGS THAT WILL STORE ARGS W/ ANY EXPANDED ENV VARIABLES */
 	char** expanded_args = (char**)calloc(echo_argc, sizeof(char*));
 	char* temp = NULL;
 	for(i = 0; i < echo_argc; i++, j++) {
		/* CHECK IF ARG IS AN ENV VAR */
 		if(args[i+1][0]=='$' && args[i+1][1]!='\0'){
			/* CHECK IF THE ARG WAS $? AND IF SO RETURN RET_STATUS */
 			if(args[i+1][1]=='?' && strlen(args[i+1])==2){
 				expanded_args[j] = (char*)calloc(2, 1);
 				sprintf(expanded_args[j],"%d",ret);
				continue;
 			}
			/* PARSE THE POSSIBLE ENV VARIABLE NAME */
 			temp = (char*)calloc(strlen(args[i+1]),1);
			/* COPY THE VARIABLE NAME W/O THE '$' */
 			strcpy(temp, args[i+1]+1);
			/* CHECK IF VAR NAME EXISTS */
 			if(getenv(temp)==NULL){
				/* ENV VAR DOESN'T EXIST */
 				fprintf(stderr,"VARIABLE DOES NOT EXIST ");
				/* FREE TEMP AND CONSIDER THIS ARG AS NON-EXISTENT BY DECREMENTING J */
 				free(temp);
 				j--;
				/* KEEP TRACK OF BAD ARGS TO SUBTRACT FROM ECHO_ARGC LATER */
 				bad_vars++;
 				continue;
 			}
			/* ADD THE EXPANDED ENV VAR TO EXPANDED_ARGS */
 			expanded_args[j] = (char*)calloc(strlen(getenv(temp))+1, 1);
 			strcpy(expanded_args[j], getenv(temp));
			/* FREE TEMP */
 			free(temp);
 		} 
 		else{
			/* NORMAL ARG TO ECHO. ADD IT TO EXPANDED_ARGS */
 			expanded_args[j] = (char*)calloc(strlen(args[i+1])+1, 1);
 			strcpy(expanded_args[j], args[i+1]);
 		}
 	}
	/* MODIFY ECHO_ARGC TO REFLECT BAD ARGS */
 	echo_argc -= bad_vars;


	/* FIND THE ECHO STR LENGTH. INC BY 1 FOR A SPACE EACH TIME */
 	for(i = 0;i<echo_argc;i++, ++echo_len){
 		echo_len += strlen(expanded_args[i]);
 	}
	/* ALLOCATE MEMORY FOR ECHO STRING */
 	echo_str = (char*)calloc(echo_len, 1);
	/* CONCATONATE ECHO STRING */
 	for(i=0;i<echo_argc;i++){
 		strcat(echo_str, expanded_args[i]);
		/* PLACE A SPACE BETWEEN EACH WORD */
 		strcat(echo_str, " ");
 	}
	/* WRITE ECHO STRING AND NEW LINE */
 	write(1,echo_str,strlen(echo_str));
 	write(1, "\n", 1);
	/* FREE echo_str */
 	free(echo_str);
 	for(i = 0; i < echo_argc; i++) {
 		free(expanded_args[i]);
 	}
 	free(expanded_args);
 	exit(0);
 }

/*
 * Fork wrapper function
 * @return pid_t - pid of the the child to the parent. O/w 0 to the child
 * -- this is a wrapper function for the fork() function. It handles any
 *    fork error and prints to stderr accordingly
 */
 pid_t Fork(){
 	pid_t pid;
 	if((pid = fork()) < 0)
		/* FORK WAS UNSUCCESSFUL. PRINT ERROR */
 		fprintf(stderr, "%s: %s\n", "Fork Error! ", strerror(errno));
	/* FORK WAS SUCCESSFUL. RETURN PID */
 	return pid;

 }

/*
 * set_cmd function - changes the existing path with
 * the new path, otherwise creates a new name and
 * path.
 *
 * @param shell_args - a double pointer shell_args. 
 */
 void set_cmd(int shell_argc, char** shell_args) {
 	int i = 1;
 	int j = 0;
 	int flag = FALSE;
 	int nameCount = 0;
 	int pathCount = 0;
 	char *path;

 	if(shell_args[1][0] == '='){
 		fprintf(stderr, "%s\n", "This is an invalid input.");
 		ret_status = 1;
 		return;
 	}

        /* COUNT LENGTH OF ENV NAME */
 	while(shell_args[1][j] != '\0') {
 		if(shell_args[1][j] == '=' && shell_args[1][j + 1] != '\0') {
 			flag = TRUE;
 			break;
 		} else if(shell_args[1][j] != '=') {
 			nameCount++;
 		}
 		j++;
 	}
        /* CHECK IF THERE IS A SPACE */
 	if(flag == FALSE)
 		i++;

 	if(shell_argc <= 3 && shell_argc != i + 1) {
 		fprintf(stderr, "%s\n", "This is an invalid input.");
 		ret_status = 1;
 		return;
 	}

        /* SAVE ENV NAME */
 	if(!flag) {
 		j = 0;
 	}
 	char *name = calloc(1, nameCount);
 	strncat(name, shell_args[1], nameCount);

        /* CHECK FOR ANY MORE SPACES*/
 	if(flag == FALSE) {
 		if(shell_args[2][0] == '=' && shell_args[2][1] == '\0') {
 			i++;
 		}
 	}
        /* COUNT LENGTH OF PATH */
 	if(shell_argc != i + 1) {
 		fprintf(stderr, "%s\n", "This is an invalid input.");
 		free(name);
 		ret_status = 1;
 		return;
 	}
 	while(shell_args[i][j] != '\0') {
 		if(shell_args[i][j] != '=') {
 			pathCount++;
 		}
 		j++;
 	}
       
    /* SAVE ENV PATH */
 	path = calloc(pathCount,1);
 	if(!flag) {
 		j = 0;
 	}
 	if(shell_args[i][j] == '=') {
 		strncat(path, shell_args[i] + 1, pathCount);
 	} else if(flag) {
 		strncat(path, shell_args[i] + nameCount + 1, pathCount);
 	} else {
 		strncat(path, shell_args[i], pathCount);
 	}
 	if(setenv(name, path, 1)) {
 		write(1,getenv("PATH"),strlen(getenv("PATH")));
 		printf("\n");
 	}

 	free(name);
 	free(path);
 	ret_status = 0;
 }

 void allocate_history(Line*** lines){
	int z;
	/* INITIALIZE STORAGE FOR COMMAND LINE HISTORY CONNECT THE LIST */
	*lines = (Line**)calloc(sizeof(Line*), 500);
	for(z = 0; z < 500; z++) {
		(*lines)[z] = (Line*)calloc(1,sizeof(Line));
		(*lines)[z]->str = (char*)calloc(MAX_INPUT, 1);
		(*lines)[z]->num = z;
		if(z > 0){
			(*lines)[z]->prev_line = (*lines)[z-1];
			(*lines)[z-1]->next_line = (*lines)[z];
		}
	}
	/* MAKE THE LIST CIRCULAR */
	(*lines)[0]->prev_line = (*lines)[499];
	(*lines)[499]->next_line = (*lines)[0];
}

/*
 * get_history function
 * @param char* history_path - path to history file
 * @returns int - fd to the file with the history. o/w returns NULL
 */
 FILE* get_fd(char* path, char* priv){
 	FILE* fd = NULL;
 	/* CHECK IF HISTORY FILE ALREADY EXISTS */
 	if(findFile(path)){
 		/* IF IT EXISTS THEN WE WILL WANT TO READ FROM IT */
 		fd = fopen(path,priv);
 	}
 	else{
 		/* WE ONLY NEEDED TO READ IT SO DON'T WORRY ABOUT IT FOR NOW */
 		fd = NULL;
 	}

 	return fd;
 }

 int copy_history(FILE* history_fd, Line** lines){
 	char lastchar;
 	/* i WILL BE RETURNED AS LINE COUNT - 1 */
 	int i, j;
 	i = j = 0;
 	while((lastchar = (char)getc(history_fd)) != EOF){
 		if(lastchar == '\n'){
 			i++;
 			j = 0;
 		}
 		else{
 			lines[i]->str[j] = lastchar;
 			j++;
 		}
 	}
 	/* DON'T COUNT THE LAST NEW LINE CHARACTER AT THE END IN HISTORY */
 	return i-1;
 }

 void save_history(char* history_path, int start, Line** lines){
 	/* PROGRAM ENDING. SAVE COMMAND LINE HISTORY IN FILE */
 	if(!(lines[start]->str[0]==0)){
 		FILE* history_fd = fopen(history_path,"w");
 		int m, n, o;
 		char c;
 		m = n = o = 0;
 		Line* curr = lines[start];
 		for(;n < linec-1; n++)
 			curr = curr->prev_line;
 		for(; m < linec+1; m++){
 			while((c = curr->str[o]) != '\0'){
 				putc(c, history_fd);
 				o++;
 			}
 			/* WRITE TO NEXT LINE */
 			putc('\n', history_fd);
 			/* NEXT LINE */
 			curr = curr->next_line;
 			/* RESTART STRING INDEX */
 			o = 0;
 		}
 	}
 }

 void print_history(FILE* file, int start, Line** lines){
 	/* PROGRAM ENDING. SAVE COMMAND LINE HISTORY IN FILE */
 	if(!(lines[start]->str[0]==0)){

 		int m, n, o;
 		char c;
 		int k;
 		m = n = o = 0;
 		Line* curr = lines[start];
 		for(;n < linec-1; n++)
 			curr = curr->prev_line;
 		for(; m < linec; m++){
 			k = m+1;
 			if(m<9){
 				if(k<=linec){
 					fprintf(stdout, "  ");
 					fprintf(stdout, "%d",k);
 					fprintf(stdout, " ");
 				}
 			}
 			else{
 				if(k<=linec){
 				fprintf(stdout, " ");
 				fprintf(stdout, "%d",k);
 				fprintf(stdout, " ");
 				}
 			}

 			while((c = curr->str[o]) != '\0'){
 				putc(c, file);
 				o++;
 			}
 			/* WRITE TO NEXT LINE */
 			if(k<=linec)
 				putc('\n', file);
 			/* NEXT LINE */
 			curr = curr->next_line;
 			/* RESTART STRING INDEX */
 			o = 0;
 		}
 	}
 	ret_status = 0;
 	return;
 }

 void free_history(Line** lines){
 	int p;
 	for(p = 0; p < 500; p++) {
 		free(lines[p]->str);
 		free(lines[p]);
 	}
 	free(lines);
 }
 
 /*
  * insert_spaces function
  * @params - address of string to put spaces into
  * -- This function places strings between all arguments for a shell
  */
 void insert_spaces(char** s){
 	/* SPECIAL CHARS ARE: <, >, |, "", = */
 	char* str = *s;
 	int str_len = strlen(str);
 	for(int i = 0; i < str_len; i++){
 		if(str[i] == '<' || str[i] == '>'){
 			/* CHECK IF THERE IS AN INT SPECIFIER */
 			if(i >= 1 && (str[i-1] == '0' || str[i-1] == '1' || str[i-1] == '2')){
 				/* THERE IS SO PLACE SPACE BEFORE INT AND AFTER BRACKET */
 				if(str[i+1] != ' '){
 					insert_char(&str, ' ', i+2);
 					str_len++;
 				}
 				if(i > 1 && str[i-1] != ' '){
 					insert_char(&str, ' ', i-1);
 					str_len++;
 					i++;
 				}
 			}
 			else{
 				/* THERE ISN'T SO PLACE SPACE AFTER AND BEFORE BRACKET IF IT NEEDS ONE */
 				if(str[i+1] != ' '){
 					insert_char(&str, ' ', i+1);
 					str_len++;
 				}
 				if(i > 1 && str[i-1] != ' '){
 					insert_char(&str, ' ', i);
 					str_len++;
 					i++;
 				}
 			}
 		} /* CHECK IF IT'S ANOTHER SPECIAL CHAR */
 		else if(str[i] == '|' || str[i] == '='){
 			/* PLACE A SPACE AFTER AND BEFORE SPECIAL CHAR IF IT NEEDS ONE */
 			if(str[i+1] != ' '){
 				insert_char(&str, ' ', i+1);
 				str_len++;
 			}
 			if(i > 1 && str[i-1] != ' '){
 				insert_char(&str, ' ', i);
 				str_len++;
 				i++;
 			}
 		}
 	}
 }

 void insert_char(char** s, char c, int index){

 	char* str = *s;
 	int str_len = strlen(str);
 	int i = str_len;
 	str[i+1] = '\0';
 	while(i > index){
  		str[i] = str[i-1];
 		i--;
  	}
  	str[index] = c;

 }

 void remove_char(char** s, int index){
 	char* str = *s;
 	int new_len = strlen(str);
 	/* HAVE INDEX POINT TO THE I'TH CHAR THAT IS GOING TO BE REMOVED */
 	index--;
 	while(index < new_len){
 		str[index] = str[index+1];
 		index++;
 	}
 	/* END STRING WITH NULL TERMINATOR */
 	str[index] = '\0';
 }

 int count_sargc(char* cmd){
 	/* COLON IS THE DELIMETER FOR ENVIRONMENTAL PATHS */
 	char delim[2] = " ";
 	int argc = 0;
	/* GET FIRST ARG */
	char* temp = (char*)calloc(1,strlen(cmd)+1);
	strcpy(temp,cmd);
 	char* token = strtok(temp, delim);
 	while(token != NULL && token[0] != '\n'){
 		argc++;
    	token = strtok(NULL, delim); 
 	}
 	free(temp);
 	if(argc==0 && strlen(cmd) > 0 && cmd[0] != ' ' && cmd[0] != '\n')
 		return ++argc;
 	return argc;

 }

 void init_sargs(char* cmd, char*** args){
 	
 	/* ALLOCATE MEMORY FOR SHELL_ARGS */
 	char** s_args = *args;
  	char delim[2] = " ";
 	int index = 0;
	/* GET FIRST ARG */
	char* temp = (char*)calloc(1,strlen(cmd)+1);
	strcpy(temp,cmd);
 	char* token = strtok(temp, delim);
 	while(token != NULL && token[0] != '\n'){
 		s_args[index] = (char*)calloc(1, strlen(token)+1);
 		strcpy(s_args[index], token);
    	token = strtok(NULL, delim); 
    	index++;
 	}
 	free(temp);
 }

/*
 * change_fd function
 * @params FILE* file - the fd that is being duplicated
 *         int std - which std stream is being overwritten
 */
 void change_fd(FILE* file, int std){
 	/* MODIFY STDIN "<" */
 	if(std == 0){
 		dup2(fileno(file), STDIN_FILENO);
 	}
 	/* MODIFY STDOUT ">" */
 	else if(std == 1){
 		dup2(fileno(file), STDOUT_FILENO);
 	}
 	/* MODIFY STDERR "2>" */
 	else{
 	 dup2(fileno(file), STDERR_FILENO);
 	}
 	fclose(file);
 	return;
 }

/*
 * redirect function
 * @param  char** shell_args - the shell args to parse for special chars
 *         int argc          - the number of args to look through
 * -- this function will redirect either stdin, stdout, or stderr depending 
 *    on any special characters found in the args
 */
 int redirect(char** shell_args, int argc){
 	int j;
 	FILE* fd = NULL;
  	for(j = 1; j < argc - 1; j++) {
 		if(!(strcmp(shell_args[j], "<")) || !(strcmp(shell_args[j], "0<"))) {
 			/* MODIFY STDIN FD. MUST CHECK IF IT EXISTS */
 			if((fd = get_fd(shell_args[j+1], "w")) != NULL)
				change_fd(fd, 0);
			else{
				fprintf(stderr, "320sh: %s: No such file or directory\n", shell_args[j+1]);
				return -1;
			}
 		} /* MODIFY STDOUT */
 		else if(!(strcmp(shell_args[j], ">")) || !(strcmp(shell_args[j], "1>"))) {
 			if((fd = fopen(shell_args[j+1], "w")) != NULL){
				change_fd(fd, 1);
 			}
 		} /* MODIFY STDERR */
 		else if(!(strcmp(shell_args[j], "2>"))) {
 			if((fd = fopen(shell_args[j+1], "w")) != NULL)
				change_fd(fd, 2);
 		}
 	}
 	return 0;
 }

 char** get_child_args(char **shell_args, int argc){
 	int i;
 	int j=0;
 	char **ptr = calloc(sizeof(char*), argc);
 	for(i = 0; i < argc; i++) {
 		if(strcmp(shell_args[i], "<") && strcmp(shell_args[i], "0<") && strcmp(shell_args[i], ">") 
 			&& strcmp(shell_args[i], "1>") && strcmp(shell_args[i], "2>")) {

 			ptr[j] = calloc(strlen(shell_args[i]) + 1, 1);
 			strcpy(ptr[j], shell_args[i]);
 			j++;
 		}
 		else
 			i++;
 	}
 	ptr[j] = NULL;
 	return ptr;

 }

 int get_child_arc(char** child_args){
 	int i=0;
 	while(child_args[i]!=NULL){
 		i++;
 	}
 	return i;
 }

 void help_cmd(){
 	fprintf(stdout,"These shell commands are defined internally. Type 'help' to see this list\n\n"
 		"cd [-] | NEW_DIR\tChanges current directory.\n\t\t\t[-] will change to the last directory."
 		"\n\t\t\tNEW_DIR will modify the current directory to NEW_DIR.\n\t\t\tIf no argument is given, the current directory\n\t\t\t"
 		"will change to the user's home directory.\n\n"
 		"pwd\t\t\tPrint absolute path of current location on file system.\n\n"
 		"echo STRING | ENV_VAR\tBasic echo support. Print strings and expand\n\t\t\tenvironmental variables.\n\n"
 		"set VAR_NAME = VALUE\tModify existing environmental variables and\n\t\t\tcreate new ones.\n\n"
 		"history\t\t\tDisplays the command line history.\n\n"
 		"clear-history\t\tClears the command line history.\n\n"
 		"exit\t\t\tExits the shell.\n\n"
 		"help\t\t\tPrint list of all builtin commands and\n\t\t\ttheir basic usage.\n");
 	ret_status = 0;
 	return;
 }
