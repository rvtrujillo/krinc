#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <fcntl.h>

// ASSUME NO INPUT LINE WILL BE LONGER THAN 1024 BYTES
#define MAX_INPUT 1024
#define TRUE	1
#define FALSE	0

/* STRUCT TO CREATE A LINKED LIST FOR COMMAND LINE HISTORY */
typedef struct Line{
	struct Line* prev_line;
	struct Line* next_line;
	char* str;
	int num;
}Line;

void findNextChar(char *cursor, char *cmd, char **shell_args);
int execvpe(const char *file, char *const argv[], char *const envp[]);

int findFile(char* fileName);
pid_t Fork();
char* getDir();

typedef void sig_handler(int);
sig_handler *signal(int, sig_handler*);
void sigint_handler(int signum);

void set_cmd(int shell_argc, char** shell_args);
void pwd_cmd();
void cd_cmd(int argc, char** args);
void echo_cmd(int arg_count, char** args, int ret);
void help_cmd();

FILE* get_fd(char* path, char* priv);

int copy_history(FILE* history_fd, Line** lines);
void allocate_history(Line*** lines);
void save_history(char* history_path, int start, Line** lines);
void print_history(FILE* file, int start, Line** lines);
void free_history(Line** lines);

void insert_spaces(char** s);
void insert_char(char** s, char c, int index);
void remove_char(char** s, int index);

int count_sargc(char* cmd);
void init_sargs(char* cmd, char*** args);

void change_fd(FILE* file, int std);
int redirect(char** shell_args, int argc);

char** get_child_args(char **shell_args, int argc);
int get_child_arc(char** child_args);