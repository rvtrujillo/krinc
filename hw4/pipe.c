#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <fcntl.h>

// Assume no input line will be longer than 1024 bytes
#define MAX_INPUT 1024
#define TRUE	1
#define FALSE	0

struct stat checkPath;
char* prev_path;
char* current_path;
int ret_status;
int countPipes;

void findNextChar(char *cursor, char *cmd, char **shell_args);
int execvpe(const char *file, char *const argv[], char *const envp[]);
int findFile(char* fileName);
pid_t Fork();
char* getDir();
typedef void sig_handler(int);
sig_handler *signal(int, sig_handler*);
void sigint_handler(int signum);

void set_cmd(int shell_argc, char** shell_args);
void help_cmd();
void pwd_cmd();
void cd_cmd(int argc, char** args);
void echo_cmd(int arg_count, char** args);
FILE* get_historyfd(char* history_path);
void insert_char(char** s, char c, int index);

typedef struct Line{
	struct Line* prev_line;
	struct Line* next_line;
	char* str;
	int num;
}Line;
int linec, currentLineIndex, printLineIndex = 0;
Line** lines;
char* current_line;

void allocate_history(Line*** lines){
	int z;
	/* INITIALIZE STORAGE FOR COMMAND LINE HISTORY CONNECT THE LIST */
	*lines = (Line**)calloc(sizeof(Line*), 500);
	for(z = 0; z < 500; z++) {
		(*lines)[z] = (Line*)calloc(1,sizeof(Line));
		(*lines)[z]->str = (char*)calloc(MAX_INPUT, 1);
		(*lines)[z]->num = z;
		if(z > 0){
			(*lines)[z]->prev_line = (*lines)[z-1];
			(*lines)[z-1]->next_line = (*lines)[z];
		}
	}
	/* MAKE THE LIST CIRCULAR */
	(*lines)[0]->prev_line = (*lines)[499];
	(*lines)[499]->next_line = (*lines)[0];
}

/*
 * get_history function
 * @param char* history_path - path to history file
 * @returns int - fd to the file with the history. o/w returns NULL
 */
 FILE* get_fd(char* path, char* priv){
 	FILE* fd = NULL;
 	/* CHECK IF HISTORY FILE ALREADY EXISTS */
 	if(findFile(path)){
 		/* IF IT EXISTS THEN WE WILL WANT TO READ FROM IT */
 		fd = fopen(path,priv);
 	}
 	else{
 		/* WE ONLY NEEDED TO READ IT SO DON'T WORRY ABOUT IT FOR NOW */
 		fd = NULL;
 	}

 	return fd;
 }

 int copy_history(FILE* history_fd, Line** lines){
 	char lastchar;
 	/* i WILL BE RETURNED AS LINE COUNT - 1 */
 	int i, j;
 	i = j = 0;
 	while((lastchar = (char)getc(history_fd)) != EOF){
 		if(lastchar == '\n'){
 			//lines[i]->str[j] = '\0';
 			i++;
 			j = 0;
 		}
 		else{
 			lines[i]->str[j] = lastchar;
 			j++;
 		}
 	}
 	/* DON'T COUNT THE LAST NEW LINE CHARACTER AT THE END IN HISTORY */
 	return i-1;
 }

 void save_history(char* history_path, int start, Line** lines){
 	/* PROGRAM ENDING. SAVE COMMAND LINE HISTORY IN FILE */
 	if(!(lines[start]->str[0]==0)){
 		FILE* history_fd = fopen(history_path,"w");
 		int m, n, o;
 		char c;
 		m = n = o = 0;
 		Line* curr = lines[start];
 		for(;n < linec-1; n++)
 			curr = curr->prev_line;
 		for(; m < linec+1; m++){
 			while((c = curr->str[o]) != '\0'){
 				putc(c, history_fd);
 				o++;
 			}
 			/* WRITE TO NEXT LINE */
 			putc('\n', history_fd);
 			/* NEXT LINE */
 			curr = curr->next_line;
 			/* RESTART STRING INDEX */
 			o = 0;
 		}
 	}
 }

 void print_history(FILE* file, int start, Line** lines){
 	/* PROGRAM ENDING. SAVE COMMAND LINE HISTORY IN FILE */
 	if(!(lines[start]->str[0]==0)){

 		int m, n, o;
 		char c;
 		int k;
 		m = n = o = 0;
 		Line* curr = lines[start];
 		for(;n < linec-1; n++)
 			curr = curr->prev_line;
 		for(; m < linec+1; m++){
 			k = m+1;
 			if(m<9){
 				if(k<=linec){
 					fprintf(stdout, "  ");
 					fprintf(stdout, "%d",k);
 					fprintf(stdout, " ");
 				}
 			}
 			else{
 				if(k<=linec){
 					fprintf(stdout, " ");
 					fprintf(stdout, "%d",k);
 					fprintf(stdout, " ");
 				}
 			}

 			while((c = curr->str[o]) != '\0'){
 				putc(c, file);
 				o++;
 			}
 			/* WRITE TO NEXT LINE */
 			if(k<=linec)
 				putc('\n', file);
 			/* NEXT LINE */
 			curr = curr->next_line;
 			/* RESTART STRING INDEX */
 			o = 0;
 		}
 		//fprintf(stdout, "\b\b\b \b");

 	}
 }

 void free_history(Line** lines){
 	int p;
 	for(p = 0; p < 500; p++) {
 		free(lines[p]->str);
 		free(lines[p]);
 	}
 	free(lines);
 }
 
 /*
  * insert_spaces function
  * @params - address of string to put spaces into
  * -- This function places strings between all arguments for a shell
  */
  void insert_spaces(char** s){
 	/* SPECIAL CHARS ARE: <, >, |, "", = */
  	char* str = *s;
  	int str_len = strlen(str);
  	countPipes = 0;
  	for(int i = 0; i < str_len; i++){
  		if(str[i] == '<' || str[i] == '>'){
 			/* CHECK IF THERE IS AN INT SPECIFIER */
  			if(i >= 1 && (str[i-1] == '0' || str[i-1] == '1' || str[i-1] == '2')){
 				/* THERE IS SO PLACE SPACE BEFORE INT AND AFTER BRACKET */
  				if(str[i+1] != ' '){
  					insert_char(&str, ' ', i+2);
  					str_len++;
  				}
  				if(i > 1 && str[i-1] != ' '){
  					insert_char(&str, ' ', i-1);
  					str_len++;
  					i++;
  				}
  			}
  			else{
 				/* THERE ISN'T SO PLACE SPACE AFTER AND BEFORE BRACKET IF IT NEEDS ONE */
  				if(str[i+1] != ' '){
  					insert_char(&str, ' ', i+1);
  					str_len++;
  				}
  				if(i > 1 && str[i-1] != ' '){
  					insert_char(&str, ' ', i);
  					str_len++;
  					i++;
  				}
  			}
 		} /* CHECK IF IT'S ANOTHER SPECIAL CHAR */
  			else if(str[i] == '|'){
 			/* PLACE A SPACE AFTER AND BEFORE SPECIAL CHAR IF IT NEEDS ONE */
  				if(str[i+1] != ' '){
  					insert_char(&str, ' ', i+1);
  					str_len++;
  				}
  				if(i > 1 && str[i-1] != ' '){
  					insert_char(&str, ' ', i);
  					str_len++;
  					i++;
  				}
  				countPipes++;
  			} else if(str[i] == '=') {
 			/* PLACE A SPACE AFTER AND BEFORE SPECIAL CHAR IF IT NEEDS ONE */
  				if(str[i+1] != ' '){
  					insert_char(&str, ' ', i+1);
  					str_len++;
  				}
  				if(i > 1 && str[i-1] != ' '){
  					insert_char(&str, ' ', i);
  					str_len++;
  					i++;
  				}
  			}
  		}
  	}

  	void insert_char(char** s, char c, int index){

  		char* str = *s;
  		int str_len = strlen(str);
  		int i = str_len;
  		str[i+1] = '\0';
  		while(i > index){
  			str[i] = str[i-1];
  			i--;
  		}
  		str[index] = c;

  	}

  	void remove_char(char** s, int index){
  		char* str = *s;
  		int new_len = strlen(str);
 	/* HAVE INDEX POINT TO THE I'TH CHAR THAT IS GOING TO BE REMOVED */
  		index--;
  		while(index < new_len){
  			str[index] = str[index+1];
  			index++;
  		}
 	/* END STRING WITH NULL TERMINATOR */
  		str[index] = '\0';
  	}

  	int count_sargc(char* cmd){
 	/* COLON IS THE DELIMETER FOR ENVIRONMENTAL PATHS */
  		char delim[2] = " ";
  		int argc = 0;
	/* GET FIRST ARG */
  		char* temp = (char*)calloc(1,strlen(cmd)+1);
  		strcpy(temp,cmd);
  		char* token = strtok(temp, delim);
  		while(token != NULL && token[0] != '\n'){
  			argc++;
  			token = strtok(NULL, delim); 
  		}
  		if(argc==0 && strlen(cmd) > 0 && cmd[0] != ' ' && cmd[0] != '\n')
  			return ++argc;
  		return argc;

  	}

  	void init_sargs(char* cmd, char*** args){

 	/* ALLOCATE MEMORY FOR SHELL_ARGS */
  		char** s_args = *args;
  		char delim[2] = " ";
  		int index = 0;
	/* GET FIRST ARG */
  		char* temp = (char*)calloc(1,strlen(cmd)+1);
  		strcpy(temp,cmd);
  		char* token = strtok(temp, delim);
  		while(token != NULL && token[0] != '\n'){
  			s_args[index] = (char*)calloc(1, strlen(token)+1);
  			strcpy(s_args[index], token);
  			token = strtok(NULL, delim); 
  			index++;
  		}

  	}

/*
 * change_fd function
 * @params FILE* file - the fd that is being duplicated
 */
 int change_fd(FILE* file, int std){
 	/* MODIFY STDIN "<" */
 	if(std == 0)
 		return dup2(fileno(file), STDIN_FILENO);
 	/* MODIFY STDOUT ">" */
 	else if(std == 1)
 		return dup2(fileno(file), STDOUT_FILENO);
 	/* MODIFY STDERR "2>" */
 	else return dup2(fileno(file), STDERR_FILENO);
 }

/*
 * redirect function
 * @param  char** shell_args - the shell args to parse for special chars
 *         int argc          - the number of args to look through
 * -- this function will redirect either stdin, stdout, or stderr depending 
 *    on any special characters found in the args
 */
 void redirect(char** shell_args, int argc){
 	int j;
 	FILE* fd = NULL;
 	//fprintf(stderr, "THIS IS HAPPENEING\n");
 	for(j = 1; j < argc - 1; j++) {
 		if(!(strcmp(shell_args[j], "<")) || !(strcmp(shell_args[j], "0<"))) {
 			/* MODIFY STDIN FD. MUST CHECK IF IT EXISTS */
 			if((fd = get_fd(shell_args[j+1], "w")) != NULL)
 				change_fd(fd, 0);
 		} /* MODIFY STDOUT */
 			else if(!(strcmp(shell_args[j], ">")) || !(strcmp(shell_args[j], "1>"))) {
 				if((fd = fopen(shell_args[j+1], "w")) != NULL){
 					change_fd(fd, 1);
 				}
 		} /* MODIFY STDERR */
 				else if(!(strcmp(shell_args[j], "2>"))) {
 					if((fd = fopen(shell_args[j+1], "w")) != NULL)
 						change_fd(fd, 2);
 				}
 			}
 		}

 		char** get_child_args(char **shell_args, int argc) {
 			int i;
 			int j=0;
 			char **ptr = calloc(sizeof(char*), argc);
 			for(i = 0; i < argc; i++) {
 				if(strcmp(shell_args[i], "<") && strcmp(shell_args[i], "0<") && strcmp(shell_args[i], ">") 
 					&& strcmp(shell_args[i], "1>") && strcmp(shell_args[i], "2>")) {

 					ptr[j] = calloc(strlen(shell_args[i]) + 1, 1);
 				strcpy(ptr[j], shell_args[i]);
 				j++;
 			}
 			else
 				i++;
 		}
 		ptr[j] = NULL;
 		return ptr;

 	}

 	char ** pipe_child_args(char ** shell_args, int argc) {
 		int i;
 		int j=0;
 		char **ptr = calloc(sizeof(char*), argc);
 		for(i = 0; i < argc; i++) {
 			if(strcmp(shell_args[i], "|")) {

 				ptr[j] = calloc(strlen(shell_args[i]) + 1, 1);
 				strcpy(ptr[j], shell_args[i]);
 				j++;
 			}
 		}
 		ptr[j] = NULL;
 		return ptr;
 	}

 	int get_child_arc(char** child_args){
 		int i=0;
 		while(child_args[i]!=NULL){
 			i++;
 		}
 		return i;
 	}

 	void create_pipe(char** shell_args, int argc, int countPipes, char** envp) {
 	//const int commands = countPipes + 1;
 		int i;
 		int fd[countPipes * 2];
 		int first = 0;

 	/* CHECK IF THERE ARE CORRECT AMOUNT OF COMMANDS */
 		for(i = 0; i < countPipes; i++){
 			if(pipe(fd + i*2) < 0) {
 				fprintf(stderr, "%s\n", "Wrong number of commands!");
 				exit(EXIT_FAILURE);
 			}
 		}

 		pid_t c_pid;
 		int status;
 		int j;
 		for(j = 1; j < argc - 1; j++)
 			if(!(strcmp(shell_args[j], "|"))) {
 				if(first == 0) {
 					pipe(fd);
 					if((c_pid = Fork()) == 0) {
 						close(fd[(j - 1) * 2);
 							dup2(fd[j], STDOUT_FILENO);	
 						} 
 						else {
 							close(fd[1]);
 							wait(&status);
 						}
 					}
 					char** child_args = pipe_child_args(shell_args, argc);
 					execvpe(child_args[0], child_args, envp);
 				}
 			}

 			int main (int argc, char ** argv, char **envp) {

 				int finished = 0;
 				ret_status = 0;
 				int z = 0;
 				int countPtr;
 				char *prompt = "320sh> ";
 				char cmd[MAX_INPUT] = {'0'};
 				int shell_argc = 0;
 				pid_t pid = 0;
 				int opt, debug, child_status;
 				prev_path = getDir();
 				current_path = getDir();
 				char* w_dir = NULL;

	/* CREATE PATH TO COMMAND LINE HISTORY FILE IN USER'S HOME DIRECTORY */
 				char* history_path = calloc(strlen(getenv("HOME"))+19,1);
 				strcat(history_path, getenv("HOME"));
 				strcat(history_path,"/320sh_history.txt");

	/* CREATE FD TO COMMAND LINE HISTORY FILE */
 				FILE* history_fd = get_fd(history_path,"r");
	/* ALLOCATE MEMORY FOR COMMAND LINE HISTORY */
 				allocate_history(&lines);

	/* CHECK IF THERE WAS ANY HISTORY */
 				if(history_fd==NULL)
 					currentLineIndex = printLineIndex = linec = 0;
 				else{
 					linec = copy_history(history_fd, lines);
 					fclose(history_fd);
 					currentLineIndex = printLineIndex = linec;
 				}

	/* CHECK IF IN DEBUGGING MODE */
 				if ((opt = getopt(argc, argv, "d")) != -1){
 					debug = 1;
 				}
 				else debug = 0;

 	//char* cmd2 = calloc(1,1024);

 				while (!finished) {
 					errno = 0;
 					char *cursor;
 					char last_char;
 					int rv;
 					int count;
 					char * ptr;
 		//int numSpecial = 0;

    	// PRINT THE PROMPT WITH CWD
 					w_dir = (char*)calloc(1024, 1); 
 					w_dir = getDir();
 					prompt = (char*)calloc(strlen(w_dir)+14, 1);
 					strcat(prompt,"[");
 					strcat(prompt,w_dir);
 					free(w_dir);
 					strcat(prompt,"] 320sh> ");

 					rv = write(1, prompt, strlen(prompt));
 					if (!rv) { 
 						finished = 1;
 						break;
 					}

    	// READ AND PARSE THE INPUT
 					int arrow = 0;
 					int numLeft = 0;
    	//int numRight = 0;
    	//char keep[3] = {0};

    	/* KEEP TRACK OF NUMBER OF QUOTES */
 					int numQuotes=0;

 					for(rv = 1, count = 0, cursor = cmd, last_char = 1; rv && (++count < (MAX_INPUT-1))	&& (last_char != '\n'); cursor++) { 

 						rv = read(0, cursor, 1);
 						last_char = *cursor;
 						int line_len;
 			//int prompt_len;

 			/* DETECT CTRL+C */
 						if(last_char == 3) {
 				/* WRITE FUTURE PROMPT ON NEW LINE */
 							write(1, "\n", 2);

 				/* FREE LEFTOVER MEMORY */
 							free(prompt);
 							free(prev_path);
 							free(current_path);

				/* PROGRAM ENDING. SAVE COMMAND LINE HISTORY IN FILE */
 							save_history(history_path,currentLineIndex-1, lines);
				/* FREE ALL SPACE ALLOCATED FOR COMMAND MEMORY */
 							free_history(lines);

 				/* HANDLE THE CTRL+C SIGNAL - SIGINT */
 							if(signal(SIGINT, sigint_handler) == SIG_ERR)
 								fprintf(stderr, "SIGNAL ERROR\n");

 				/* IF HERE, THEN SIGNAL HANDLER FAILED. END PROGRAM W/ EXIT_FAILURE */
 							return EXIT_FAILURE;

 			} /* DETECT CTRL+A */
 							else if(last_char == 1){
 								cursor--;
				/* CHECK IF AT BEGINNING OF LINE (CAN'T GO PAST PROMPT) */ 
				/* NEED A NEW CONDITIONAL SINCE CURSOR DOESNT MOVE ANYMORE */
 								while(cursor-numLeft+1 != cmd){
					//fprintf(stderr, "\ncmd:%s\ncursor:%s\ncursor-cmd:%lu", cmd,cursor,cursor-cmd);
 									write(1,"\033[D", 3);
 									numLeft++;
					//numRight--;
					//cursor--;
 								}
 								count--;
 								continue;
 							}
 							else if(last_char == 127 || last_char == 8){
 								cursor--;
 								count--;
 								if(cursor-numLeft+1 != cmd){
 									char* temp = cmd; 
 									remove_char(&temp, count-numLeft);
 									cursor--;
 									count--;
 									for(int i = 0; i<numLeft;i++ ){
 										write(1,"\033[C", 3);
 									}
 									write(1,"\b \b", 3);

 									for(int i=0; i<numLeft; i++){
 										write(1,"\b",1);
 										write(1,cursor-i,1);
 										write(1,"\b",1);
 									}
 								}
 								continue;
 							}
			else if (last_char == 27) { // if the first value is esc
				arrow++;
				count--;
				cursor--;
				continue;
			}
			else if(last_char == '[' && arrow==1){
				arrow++;
				count--;
				cursor--;
				continue;
			}
			else if(arrow==2){
       			switch(last_char) { // THE IDENTIFIER FOR WHICH ARROW KEY WAS PRESSED
       				case 'A':
            			/* CODE FOR ARROW UP */
        				/* CHECK IF THERE IS A PREV LINE. IF NOT DO NOTHING. ELSE, MODIFY PRINTLINEINDEX */
       				if(linec<1 || ((lines[printLineIndex]->prev_line->num == currentLineIndex) && linec==500) || (linec<500 && printLineIndex==0)){
	   					/* SET VARIABLES BACK TO NORMAL VALUES */
       					cursor--;
       					count--;
       					arrow=0;
       					continue;
       				}

	   				/* INCREMENT PRINTLINEINDEX TO END IF AT ZERO */
       				if(linec==500 && printLineIndex==0 && currentLineIndex!=499){
       					printLineIndex=499;
       				}
       				else printLineIndex--;

	       			/* DELETE EVERYTHING ON THE LINE */
       				line_len = count + strlen(prompt);
       				cursor--;
       				count--;
       				while(numLeft != 0){
       					write(1,"\033[C", 3);
       					numLeft--;
        					//fprintf(stderr, "CURSOR IS MOVING RIGHT\n");
       				}
       				for(int d = 0; d < line_len; d++){
       					write(1, "\b \b", 3);
       				}

	       			/* WRITE THE PREV LINE */
       				write(1, prompt, strlen(prompt));
       				write(1, lines[printLineIndex]->str, strlen(lines[printLineIndex]->str));

	       			/* SET VARIABLES BACK TO NORMAL */
	       			/* CLEAR CMD FOR THE NEW LINE */
       				ptr = cmd;
       				for(countPtr = 0; countPtr < count - 1; countPtr++) {
       					ptr[countPtr] = '\0';
       				}
       				count = strlen(lines[printLineIndex]->str);
       				strcpy(cmd,lines[printLineIndex]->str);
       				cursor = cmd + count - 1;

       				arrow=0;
       				continue;
       				case 'B':
        			   /* CODE FOR ARROW DOWN */
        				/* CHECK IF THERE IS A NEXT LINE. IF NOT DO NOTHING */
       				if(linec<2 || (printLineIndex == currentLineIndex)){
	   					/* SET VARIABLES BACK TO NORMAL VALUES */
       					arrow=0;
       					cursor--;
       					count--;
       					continue;
       				}
       				printLineIndex = lines[printLineIndex]->next_line->num;
	       			/* DELETE EVERYTHING ON THE LINE */
       				line_len = count + strlen(prompt);
       				cursor--;
       				count--;
        				/* IF CURSOR IS NOT AT END PUT AT END AND THEN DELETE LINE */
       				while(numLeft != 0){
       					write(1,"\033[C", 3);
       					numLeft--;
       				}
       				for(int d = 0; d < line_len; d++){
       					write(1, "\b \b", 3);
       				}

       				write(1, prompt, strlen(prompt));
       				write(1, lines[printLineIndex]->str, strlen(lines[printLineIndex]->str));

	       			/* CLEAR CMD FOR THE NEW LINE */
       				ptr = cmd;
       				for(countPtr = 0; countPtr < count - 1; countPtr++) {
       					ptr[countPtr] = '\0';
       				}


       				count = strlen(lines[printLineIndex]->str);
       				strcpy(cmd,lines[printLineIndex]->str);
       				cursor = cmd + count - 1;
       				arrow=0;
       				continue;
       				case 'C':
            			/* CODE FOR ARROW RIGHT */

       				cursor--;
       				count--;
        				/* NEED A NEW CONDITIONAL SINCE CURSOR DOESNT MOVE ANYMORE */
       				if(numLeft != 0){
       					write(1,"\033[C", 3);
        					//cursor++;
        					//numRight++;
       					numLeft--;
       				}

       				arrow = 0;
        				/* CHECK IF AT END OF LINE */
       				continue;
       				case 'D':
           				/* CODE FOR ARROW LEFT */

       				cursor--;
        				/* CHECK IF AT BEGINNING OF LINE (CAN'T GO PAST PROMPT) */ 
        				/* NEED A NEW CONDITIONAL SINCE CURSOR DOESNT MOVE ANYMORE */
       				if(cursor-numLeft+1 != cmd){
        					//fprintf(stderr, "\ncmd:%s\ncursor:%s\ncursor-cmd:%lu", cmd,cursor,cursor-cmd);
       					write(1,"\033[D", 3);
       					numLeft++;
        					//numRight--;
        					//cursor--;
       				}
       				arrow=0;
       				count--;
       				continue;
       			}
       		}
       		else {
       			if(last_char == '"')
       				numQuotes++;
        		//if(last_char == '<' || )

       			arrow = 0;

        		/* CHECK IF WRITING A CHARACTER IN THE MIDDLE SOMEWHERE */
        		//if(cursor-cmd != count-1){
        			/* SHIFT EVERYTHING OVER BY 1 */
        			// char* temp = cmd + count -1;
        			// while(temp-cursor > 0){
        			// 	temp[0] = temp[-1];
        			// 	temp--;
        			// }
        			/* NOW WRITE CURSOR FROM NEW CHAR TO END */
        		// 	temp = cursor;
        		// 	write(1,temp, cmd+count-cursor-1);
        		// 	for(int d=0; d < cmd+count-cursor-2; d++)
        		// 		write(1, "\033[D", 3);

        		// }
        		//else 
       			write(1, &last_char, 1);
       		}

			/* KEEP TRACK OF THE AMOUNT OF ARGUMENTS. IF YOU READ A CHARACTER THERE'S AT LEAST 1 ARG */
         	//if(count==1 && last_char!='\n')
         	//	shell_argc=1;
			/* IF THERE IS A SPACE AND THE PREV CHAR WASN'T ALSO A SPACE. THERE'S ANOTHER ARG */
         	//if(last_char==' ' && count>1 && cursor[-1]!=' ')
         	//	shell_argc++;
			/* WELL, THERE WAS ANOTHER ARG ASSUMING THE NEXT CHAR ISN'T A NEW LINE CHAR */
         	//if(count>1 && last_char=='\n' && cursor[-1]==' ')
         	//	shell_argc--;
       	}

		/* IF USER TYPED IN NOTHING, DO NOTHING AND PROMPT AGAIN */
        //if(shell_argc==0){
       	//	continue;
       	//}



		/* BEGIN PARSING SHELL ARGS */
       	cursor = cmd;
		/* GET RID OF NEW LINE CHARACTER AT THE END */
       	cursor[strlen(cursor)-1] = '\0';
       	insert_spaces(&cursor);
       	shell_argc = count_sargc(cursor);
       	if(shell_argc == 0)
       		continue;
		/* INIT AND FILL SHELL_ARGS. +1 FOR NULL TERMINATING ARG AT END */
       	char** shell_args = (char**)calloc(shell_argc + 1, sizeof(char*));
       	init_sargs(cursor, &shell_args);
       	shell_args[shell_argc] = NULL;
       	cursor = cmd;

		/* INITIALIZE DOUBLE POINTER SHELL_ARGS */
		//char** shell_args = (char**)calloc(count, sizeof(char*));
		// for(z = 0; z < shell_argc; z++) {
		// 	shell_args[z] = (char*)calloc(count, 1);
		// }
		//fprintf(stderr, "\nTHIS IS THE NEW STRING: %s\n", cursor);



		//insertChar(&cursor, 'X', 6);
		//fprintf(stderr, "\n%s\n\n", cursor);


		/* CREATE INDEX VARIABLES FOR DOUBLE ARRAY OF ARGS */
		// int i = 0;
		// int j = 0;

		/* USER DIDN'T ENTER ENOUGH INPUT B/C THERE IS ONLY ONE QUOTE */
		// if(numQuotes == 1){
		// 	/* CREATE PROMPT FOR MORE INPUT */
		// 	free(prompt);
		// 	prompt = calloc(3,1);
		// 	strcpy(prompt, "> ");
		// 	/* WAIT FOR ANOTHER QUOTE */
		// 	while(numQuotes != 2){
		// 		write(1, prompt, strlen(prompt));
		// 		while(last_char != '\n' && count < MAX_INPUT-1){
		// 			read(0, ++cursor, 1);
		// 			last_char = *cursor;

		// 		}

		// 	}

		// }