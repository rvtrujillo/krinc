#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <fcntl.h>

// Assume no input line will be longer than 1024 bytes
#define MAX_INPUT 1024
#define TRUE	1
#define FALSE	0

struct stat checkPath;
char* prev_path;
char* current_path;
int ret_status;

void findNextChar(char *cursor, char *cmd, char **shell_args);
int execvpe(const char *file, char *const argv[], char *const envp[]);
int findFile(char* fileName);
pid_t Fork();
char* getDir();
typedef void sig_handler(int);
sig_handler *signal(int, sig_handler*);
void sigint_handler(int signum);

void set_cmd(int shell_argc, char** shell_args);
void help_cmd();
void pwd_cmd();
void cd_cmd(int argc, char** args);
void echo_cmd(int arg_count, char** args);
FILE* get_historyfd(char* history_path);

typedef struct Line{
	struct Line* prev_line;
	struct Line* next_line;
	char* str;
	int num;
}Line;
int linec, currentLineIndex, printLineIndex = 0;
Line** lines;
char* current_line;

void allocate_history(Line*** lines){
	int z;
	/* INITIALIZE STORAGE FOR COMMAND LINE HISTORY CONNECT THE LIST */
	*lines = (Line**)calloc(sizeof(Line*), 50);
	for(z = 0; z < 50; z++) {
		(*lines)[z] = (Line*)calloc(1,sizeof(Line));
		(*lines)[z]->str = (char*)calloc(MAX_INPUT, 1);
		(*lines)[z]->num = z;
		if(z > 0){
			(*lines)[z]->prev_line = (*lines)[z-1];
			(*lines)[z-1]->next_line = (*lines)[z];
		}
	}
	/* MAKE THE LIST CIRCULAR */
	(*lines)[0]->prev_line = (*lines)[49];
	(*lines)[49]->next_line = (*lines)[0];
}

/*
 * get_history function
 * @param char* history_path - path to history file
 * @returns int - fd to the file with the history. o/w returns NULL
 */
 FILE* get_historyfd(char* history_path){
 	FILE* history_fd = NULL;
 	/* CHECK IF HISTORY FILE ALREADY EXISTS */
 	if(findFile(history_path)){
 		/* IF IT EXISTS THEN WE WILL WANT TO READ FROM IT */
 		history_fd = fopen(history_path,"r");
 	}
 	else{
 		/* WE ONLY NEEDED TO READ IT SO DON'T WORRY ABOUT IT FOR NOW */
 		history_fd = NULL;
 	}

 	return history_fd;
 }

 int copy_history(FILE* history_fd, Line** lines){
 	char lastchar;
 	/* i WILL BE RETURNED AS LINE COUNT - 1 */
 	int i, j;
 	i = j = 0;
 	while((lastchar = (char)getc(history_fd)) != EOF){
 		if(lastchar == '\n'){
 			//lines[i]->str[j] = '\0';
 			i++;
 			j = 0;
 		}
 		else{
 			lines[i]->str[j] = lastchar;
 			j++;
 		}
 	}
 	/* DON'T COUNT THE LAST NEW LINE CHARACTER AT THE END IN HISTORY */
 	return i-1;
 }

 void save_history(char* history_path, int start, Line** lines){
 	/* PROGRAM ENDING. SAVE COMMAND LINE HISTORY IN FILE */
 	if(!(lines[start]->str[0]==0)){
 		FILE* history_fd = fopen(history_path,"w");
 		int m, n, o;
 		char c;
 		m = n = o = 0;
 		Line* curr = lines[start];
 		for(;n < linec-1; n++)
 			curr = curr->prev_line;
 		for(; m < linec+1; m++){
 			while((c = curr->str[o]) != '\0'){
 				putc(c, history_fd);
 				o++;
 			}
 			/* WRITE TO NEXT LINE */
 			putc('\n', history_fd);
 			/* NEXT LINE */
 			curr = curr->next_line;
 			/* RESTART STRING INDEX */
 			o = 0;
 		}
 	}
 }

 void print_history(FILE* file, int start, Line** lines){
 	/* PROGRAM ENDING. SAVE COMMAND LINE HISTORY IN FILE */
 	if(!(lines[start]->str[0]==0)){

 		int m, n, o;
 		char c;
 		m = n = o = 0;
 		Line* curr = lines[start];
 		for(;n < linec-1; n++)
 			curr = curr->prev_line;
 		for(; m < linec+1; m++){
 			while((c = curr->str[o]) != '\0'){
 				putc(c, file);
 				o++;
 			}
 			/* WRITE TO NEXT LINE */
 			putc('\n', file);
 			/* NEXT LINE */
 			curr = curr->next_line;
 			/* RESTART STRING INDEX */
 			o = 0;
 		}
 	}
 }

 void free_history(Line** lines){
 	int p;
 	for(p = 0; p < 50; p++) {
 		free(lines[p]->str);
 		free(lines[p]);
 	}
 	free(lines);
 }

 int main (int argc, char ** argv, char **envp) {

 	int finished = 0;
 	ret_status = 0;
 	int z = 0;
 	int countPtr;
 	char *prompt = "320sh> ";
 	char cmd[MAX_INPUT] = {'0'};
 	int shell_argc = 0;
 	pid_t pid = 0;
 	int opt, debug, child_status;
 	prev_path = getDir();
 	current_path = getDir();
 	char* w_dir = NULL;

	/* CREATE PATH TO COMMAND LINE HISTORY FILE IN USER'S HOME DIRECTORY */
 	char* history_path = calloc(strlen(getenv("HOME"))+19,1);
 	strcat(history_path, getenv("HOME"));
 	strcat(history_path,"/320sh_history.txt");

	/* CREATE FD TO COMMAND LINE HISTORY FILE */
 	FILE* history_fd = get_historyfd(history_path);
	/* ALLOCATE MEMORY FOR COMMAND LINE HISTORY */
 	allocate_history(&lines);

	/* CHECK IF THERE WAS ANY HISTORY */
 	if(history_fd==NULL)
 		currentLineIndex = printLineIndex = linec = 0;
 	else{
 		linec = copy_history(history_fd, lines);
 		fclose(history_fd);
 		currentLineIndex = printLineIndex = linec;
 	}

	/* CHECK IF IN DEBUGGING MODE */
 	if ((opt = getopt(argc, argv, "d")) != -1){
 		debug = 1;
 	}
 	else debug = 0;

 	char* cmd2 = calloc(1,1024);

 	while (!finished) {
 		errno = 0;
 		char *cursor;
 		char last_char;
 		int rv;
 		int count;
 		char * ptr;

    	// PRINT THE PROMPT WITH CWD
 		w_dir = (char*)calloc(1024, 1); 
 		w_dir = getDir();
 		prompt = (char*)calloc(strlen(w_dir)+14, 1);
 		strcat(prompt,"[");
 		strcat(prompt,w_dir);
 		free(w_dir);
 		strcat(prompt,"] 320sh> ");

 		rv = write(1, prompt, strlen(prompt));
 		if (!rv) { 
 			finished = 1;
 			break;
 		}

    	// READ AND PARSE THE INPUT
    	int arrow = 0;
    	//char keep[3] = {0};

 		for(rv = 1, count = 0, cursor = cmd, last_char = 1; rv && (++count < (MAX_INPUT-1))	&& (last_char != '\n'); cursor++) { 
 			// if(count >= 3){
 			// 	keep[]
 			// }
 			rv = read(0, cursor, 1);
 			last_char = *cursor;

 			/* STORE MASTER STRING IN CMD SINCE THIS IS THE END */
 			if(last_char == '\n'){
 				ptr = cmd;
		    	for(countPtr = 0; countPtr < count - 1; countPtr++) {
		    		ptr[countPtr] = cmd2[countPtr];
		    	}
			}
			

 			// /* IF NORMAL CHARACTER SAVE IT TO THE MASTER CMD2 STRING */
 			// if(count > 3 && !((last_char=='A' || last_char=='B' || last_char=='C' || last_char=='D') && cursor[-1]=='[' && cursor[-2]))
 			/* STORE CHAR IN CMD2 MASTER STRING */
 			cmd2[count-1] = last_char;


 			int line_len;
 			int prompt_len;
 			/* DETECT CTRL+C */
 			if(last_char == 3) {
 				/* WRITE FUTURE PROMPT ON NEW LINE */
 				write(1, "\n", 2);
 				
 				/* FREE LEFTOVER MEMORY */
 				free(prompt);
 				free(prev_path);
 				free(current_path);

				/* PROGRAM ENDING. SAVE COMMAND LINE HISTORY IN FILE */
 				save_history(history_path,currentLineIndex-1, lines);
				/* FREE ALL SPACE ALLOCATED FOR COMMAND MEMORY */
 				free_history(lines);

 				/* HANDLE THE CTRL+C SIGNAL - SIGINT */
 				if(signal(SIGINT, sigint_handler) == SIG_ERR)
 					fprintf(stderr, "SIGNAL ERROR\n");

 				/* IF HERE, THEN SIGNAL HANDLER FAILED. END PROGRAM W/ EXIT_FAILURE */
 				return EXIT_FAILURE;
 			}
 			else if(last_char == 127 || last_char == 8){
 				line_len = count + strlen(prompt);
        		prompt_len = strlen(prompt);
 				if(line_len > prompt_len){
					write(1, "\b \b", 3);
					count -= 2;
					*(cmd + count) = 0;
					cursor = cmd + count;
				}
 			}
			else if (last_char == 27) { // if the first value is esc
				arrow++;
			}
    		else if(last_char == '[' && arrow==1){
    			arrow++;
    		}
       		else if(arrow==2){
       			switch(last_char) { // THE IDENTIFIER FOR WHICH ARROW KEY WAS PRESSED
    				case 'A':
            			/* CODE FOR ARROW UP */
        				/* CHECK IF THERE IS A PREV LINE. IF NOT DO NOTHING */
	    				if(linec<2 || ((lines[printLineIndex]->prev_line->num == currentLineIndex) && linec==50) || (linec<50 && printLineIndex==0)){
	    					arrow=0;
	    					continue;
	    				}
	    				if(linec==50 && printLineIndex==0 && currentLineIndex!=49){
	    					printLineIndex=49;
	    				}
	    				else printLineIndex--;
	        			/* DELETE EVERYTHING ON THE LINE */
	        			line_len = count + strlen(prompt);
	        			for(int d = 0; d < line_len; d++){
	        				write(1, "\b \b", 3);
	        			}

	        			/* WRITE THE PREV LINE */
	        			write(1, prompt, strlen(prompt));
	        			write(1, lines[printLineIndex]->str, strlen(lines[printLineIndex]->str));

	        			/* MODIFY MASTER STRING */
	        			free(cmd2);
	        			cmd2 = calloc(strlen(lines[printLineIndex]->str)+1,1);
	        			strcpy(cmd2, lines[printLineIndex]->str);

	        			/* SET VARIABLES BACK TO NORMAL */
	        			/* CLEAR CMD FOR THE NEW LINE */
				    	ptr = cmd;
				    	for(countPtr = 0; countPtr < count - 1; countPtr++) {
				    		ptr[countPtr] = '\0';
				    	}
				    	count = strlen(lines[printLineIndex]->str);
				    	strcpy(cmd,lines[printLineIndex]->str);
				    	cursor = cmd + count - 1;

	        			arrow=0;
    					break;
    				case 'B':
        			    /* CODE FOR ARROW DOWN */
        				/* CHECK IF THERE IS A NEXT LINE. IF NOT DO NOTHING */
	    				if(linec<2 || (printLineIndex == currentLineIndex)){
	    					arrow=0;
	    					continue;
	    				}
	    				printLineIndex = lines[printLineIndex]->next_line->num;
	        			/* DELETE EVERYTHING ON THE LINE */
	        			line_len = count + strlen(prompt);
	        			//char* temp = cursor;
	        			for(int d = 0; d < line_len; d++){
	        				write(1, "\b \b", 3);
	        			}
	        			write(1, prompt, strlen(prompt));
	        			//fprintf(stderr, "print index: %d\nstr at print index: %s\n", printLineIndex-1, lines[printLineIndex-1]->str);
	        			write(1, lines[printLineIndex]->str, strlen(lines[printLineIndex]->str));

	        			/* MODIFY MASTER STRING */
	        			free(cmd2);
	        			cmd2 = calloc(strlen(lines[printLineIndex]->str)+1,1);
	        			strcpy(cmd2, lines[printLineIndex]->str);

	        			/* SET VARIABLES BACK TO NORMAL */
	        			/* CLEAR CMD FOR THE NEW LINE */
				    	ptr = cmd;
				    	for(countPtr = 0; countPtr < count - 1; countPtr++) {
				    		ptr[countPtr] = '\0';
				    	}
				    	count = strlen(lines[printLineIndex]->str);
				    	strcpy(cmd,lines[printLineIndex]->str);
				    	cursor = cmd + count - 1;
	        			arrow=0;
        				break;
        			case 'C':
            			/* CODE FOR ARROW RIGHT */
        				cursor -= 3;
        				count -= 3;
        				char k = cmd2[cursor-cmd]; //cursor[1];
        				if(cursor < (cmd + count-1)){
        					cursor[1]=0;
        					write(1,&k,1);
        					//fprintf(stderr,"\nk is: %c", k);
          					cursor++;
        				}
        				arrow = 0;
        				/* CHECK IF AT END OF LINE */
        				break;
        			case 'D':
           				/* CODE FOR ARROW LEFT */
        				cursor -= 3;
        				/* CHECK IF AT BEGINNING OF LINE (CAN'T GO PAST PROMPT) */ 
        				if(cursor-cmd >= 0){
        					//fprintf(stderr, "\ncmd:%s\ncursor:%s\ncursor-cmd:%lu", cmd,cursor,cursor-cmd);
        					write(1,"\b", 1);
        					cursor--;
        				}
        				arrow=0;
        				count -= 3;
           				break;
        		}
       		}
        	else {
        		arrow = 0;
        		write(1, &last_char, 1);
        	}

		 	/* KEEP TRACK OF THE AMOUNT OF ARGUMENTS. IF YOU READ A CHARACTER THERE'S AT LEAST 1 ARG */
        	if(count==1 && last_char!='\n')
        		shell_argc=1;
		 	/* IF THERE IS A SPACE AND THE PREV CHAR WASN'T ALSO A SPACE. THERE'S ANOTHER ARG */
        	if(last_char==' ' && count>1 && cursor[-1]!=' ')
        		shell_argc++;
		 	/* WELL, THERE WAS ANOTHER ARG ASSUMING THE NEXT CHAR ISN'T A NEW LINE CHAR */
        	if(count>1 && last_char=='\n' && cursor[-1]==' ')
        		shell_argc--;
        }
		
		/* IF USER TYPED IN NOTHING, DO NOTHING AND PROMPT AGAIN */
        if(shell_argc==0){
       		continue;
       	}
		
		/* INITIALIZE DOUBLE POINTER SHELL_ARGS */
		char** shell_args = (char**)calloc(count, sizeof(char*));
		for(z = 0; z < shell_argc; z++) {
			shell_args[z] = (char*)calloc(count, 1);
		}

		/* BEGIN PARSING SHELL ARGS */
		cursor = cmd;
		/* CREATE INDEX VARIABLES FOR DOUBLE ARRAY OF ARGS */
		int i = 0;
		int j = 0;

		/* READ ENTIRE STRING FROM SHELL COMMAND LINE */
    	while(*cursor != '\0' && *cursor != '\n') {
			/* IF SPACE CHARACTER SKIP TO NEXT ARG ON COMMAND LINE */
			if(*cursor == ' ') {
				cursor++;
				shell_args[i][j] = '\0';
				continue;
			}
			/* CHECK CHAR BEFORE CURSOR FOR A SPACE */
    		if(!(cursor == cmd)){
				/* IF THERE IS SPACE THEN WE JUST SKIPPED TO NEXT ARG */
    			if(cursor[-1] == ' '){
    				i++;
    				j = 0;
    			}
    		}
			/* WRITE THE CHAR TO THE ARRAY AND INCREMENT */
			shell_args[i][j] = *cursor;
			j++;
			cursor++;
    	}
		/* ADD NULL TERMINATOR TO END OF LAST ARG */
    	shell_args[i][j]='\0';
		/* ADD NULL POINTER TO END OF ARRAY OF ARGS */
    	shell_args[i+1]=NULL;

    	free(lines[currentLineIndex]->str);
    	lines[currentLineIndex]->str = calloc(1, strlen(cmd)+1);
    	strncpy(lines[currentLineIndex]->str, cmd, count - 2);
    	strcat(lines[currentLineIndex]->str, "\0");

    	if(linec < 50){
    		linec++;
    		currentLineIndex++;
    		printLineIndex = currentLineIndex;
    	}
    	else if(linec == 50){
    		if(currentLineIndex==49){
    			currentLineIndex = 0;
    		}
    		else{
    			currentLineIndex++;
    		}
    		printLineIndex = currentLineIndex;
    	}

    	/* CLEAR CMD FOR THE NEXT LINE */
    	ptr = cmd;
    	for(countPtr = 0; countPtr < count - 1; countPtr++) {
    		ptr[countPtr] = '\0';
    	}

		/* CHECK IF pwd COMMAND WAS ENTERED */
		if(strcmp(shell_args[0],"pwd") == 0){
			pwd_cmd();
			continue;
		} /* CHECK IF help COMMAND WAS ENTERED */
		else if(strcmp(shell_args[0],"help") == 0){
			help_cmd();
			continue;
		} 	
		else if(strcmp(shell_args[0], "exit") == 0) {
			/* PROGRAM ENDING. SAVE COMMAND LINE HISTORY IN FILE */
			save_history(history_path,currentLineIndex-1, lines);
			/* FREE ALL SPACE ALLOCATED FOR COMMAND MEMORY */
			free_history(lines);
			exit(0);
		} /* CHECK IF cd COMMAND WAS ENTERED */
		else if(strcmp(shell_args[0],"cd") == 0){
			cd_cmd(shell_argc, shell_args);
			continue;
		} /* CHECK IF set COMMAND WAS ENTERED */
		else if(strcmp(shell_args[0], "set") == 0) {
			if(shell_args[1]!=NULL)
				set_cmd(shell_argc, shell_args);
			else
				fprintf(stderr, "Invalid input for set.\n");
			continue;
		} /* CHECK IF echo COMMAND WAS ENTERED */
		else if(strcmp(shell_args[0], "echo") == 0){
			echo_cmd(shell_argc, shell_args);
			continue;
		}
		else if(strcmp(shell_args[0], "history") == 0){
			print_history(stdout, currentLineIndex-1, lines);
			continue;
		}
		else if(strcmp(shell_args[0], "clear-history") == 0){
			free_history(lines);
			allocate_history(&lines);
			currentLineIndex = printLineIndex = linec = 0;
			continue;
		}
		/* THE FILE EXISTS SO RUN THE FILE */
		if(findFile(shell_args[0])) {
			if((pid = Fork()) == 0){
				/* IF DEBUG MODE, DISPLAY DEBUGGING INFO */
				if(debug)
					fprintf(stderr, "RUNNING: %s\n", shell_args[0]);
				/* POSSESS THE CHILD PROCESS */
				execvpe(shell_args[0], shell_args, envp);
			}
			/* HAVE THE PARENT PROCESS WAIT FOR ITS CHILD TO DIE */
			wait(&child_status);
			ret_status = 0;
			/* IF IN DEBUG MODE, PRINT EXIT STATUS OF CHILD PROCESS */
			if(debug && WIFEXITED(child_status))
				fprintf(stderr, "ENDED: %s (ret=%d)\n", shell_args[0], WEXITSTATUS(child_status));
		}
		else {
			ret_status = 1;
			fprintf(stderr, "%s : %s\n", shell_args[0], strerror(errno));
		}
	    *cursor = '\0';
	    if (!rv) { 
	    	finished = 1;
	    	break;
	    }
	    for(z = 0; z < shell_argc; z++) {
	    	free(shell_args[z]);
	    }
	    free(shell_args);
    }
	/* FREE VARIABLES AND RETURN */
	free(prompt);
	free(prev_path);
	free(current_path);
	return EXIT_SUCCESS;
}


/*
 * findFile function
 * @param char* fileName - a string that is the file name (no slash)
 * @return int - returns 0 if file doesn't exist. 1 o/w
 * -- this function will take a file name and append it to each
 *    environmental path and check whether the file exists.
 */
 int findFile(char* fileName){

	/* CHECK IF PROVIDED RELATIVE PATH ALREADY */
 	if(fileName[0] == '/') {
 		if(stat(fileName, &checkPath) == -1) {
 			return 0;
 		} else {
 			return 1;
 		}
 	}

	/* ONLY GIVEN FILE NAME. STORE STRING OF ALL ENVIRONMENTAL PATHS */
 	char* envPaths = getenv("PATH");
 	char* temp = (char*)malloc(strlen(envPaths)+1);
 	strcpy(temp,envPaths);
	/* COLON IS THE DELIMETER FOR ENVIRONMENTAL PATHS */
 	char delim[2] = ":";
	/* GET FIRST ENVIRONMENTAL PATH TO START */
 	char* token = strtok(temp, delim);
	/* STRING TO STORE EACH COMPLETE PATH */
 	char* pathToCheck;
	/* CHECK IF ANY OF THE PATHS LEAD TO A FILE THAT EXISTS */
 	while(token != NULL){
        /* ALLOCATE MEMORY FOR COMPLETE PATH TO CHECK
		 * +2 FOR NULL TERMINATOR AND FOR THE ADDED SLASH IN FRONT OF FILENAME */
 		pathToCheck = malloc(strlen(token) + strlen(fileName) + 2);
		/* COPY ENV PATH TO pathToCheck */
 		strcpy(pathToCheck, (token+4));
		/* ADD SLASH TO END OF pathToCheck */
 		strcat(pathToCheck, "/");
    	/* ADD FILENAME TO COMPLETE THE PATH */
 		strcat(pathToCheck, fileName);

    	/* WRITE PATHS FOR TESTING */
    	/* write(1, pathToCheck, 20); */
 		if(stat(pathToCheck, &checkPath) == -1) {
    		/* SET TOKEN TO NEXT PATH. KEEP LOOKING */
 			token = strtok(NULL, delim);
 		} 
 		else {
 			free(pathToCheck);
 			free(temp);
 			return 1;
 		}
 	}
	/* FILE NOT DOES NOT EXIST */
 	free(temp);
 	free(pathToCheck);
 	return 0;
 }


 void sigint_handler(int signum){
 	fflush(stdout);
 	exit(0);
 }

/* 
 * getDir function
 * Finds the current directory by calling getcwd() method.
 * @return char* - a char* pointer to the buffer which is current directory.
 */
 char* getDir(){
	/* PREPARE UPPER BOUND SIZE OF ABSOLUTE PATH */
 	size_t b_size = 4096;
	/* ALLOCATE MEMORY TO STORE ABSOLUTE PATH */
 	char* buff = (char*)malloc(b_size);
	/* STORE ABSOLUTE PATH AT buff */
 	buff = getcwd(buff, b_size);
	/* CHECK FOR ERROR*/
 	if(buff==NULL){
 		printf("Oh dear, something went wrong with getcwd()! %s\n", strerror(errno));
		//fprintf(stderr, "ENDED: pwd ");
 	}

 	return buff;
 }

/*
 * pwd_cmd function
 * Writes to the screen the absolute path, obtained by getDir(), to the current directory.
 */
 void pwd_cmd(){
	/* WRITE THE ABSOLUTE PATH TO CURRENT DIRECTORY */
 	write(1,getDir(),strlen(getDir()));
 	write(1,"\n",1);
 	free(getDir());
 	return;
 }

/*
 * cd function
 * @params int argc - arg count from shell command line
 *		   char** args - args from shell command line
 * 3 cases - 
 *		If no args after cd, then cwd becomes home directory
 *		If a '-' arg, the cwd will switch with the last cwd
 *		If anything else, cwd will move into that directory if able.
 */
 void cd_cmd(int argc, char** args){
	/* IF NO ARGS THEN SET CURRENT DIR TO HOME DIR */
 	if(argc==1){
		/* STORE current_path IN temp */
 		char* temp = (char*)malloc(strlen(current_path)+1);
 		strcpy(temp,current_path);
		/* STORE temp IN prev_path */
 		free(prev_path);
 		prev_path = (char*)malloc(strlen(temp)+1);
 		strcpy(prev_path,temp);
		/* SET current_path to HOME DIR */
 		free(current_path);
 		current_path = (char*)malloc(strlen(getenv("HOME")+1));
 		strcpy(current_path,getenv("HOME"));
		/* FREE temp AND CHANGE DIR TO HOME DIR */
 		free(temp);
 		if(chdir(getenv("HOME"))<0)
 			fprintf(stderr,"320sh: cd: %s: No such file or directory\n", args[1]);
	} /* IF '-' THEN SWITCH PREV AND CURRENT DIRECTORIES */
 		else if(args[1][0] == '-'){
		/* STORE prev_path IN temp */
 			char* temp = (char*)malloc(strlen(prev_path)+1);
 			strcpy(temp,prev_path);
		/* STORE current_path IN prev_path */
 			free(prev_path);
 			prev_path = (char*)malloc(strlen(current_path)+1);
 			strcpy(prev_path, current_path);
		/* STORE prev_path (temp) IN current_path */
 			free(current_path);
 			current_path = (char*)malloc(strlen(temp)+1);
 			strcpy(current_path,temp);
		/* FREE temp */
 			free(temp);
 			if(chdir(current_path)<0)
 				fprintf(stderr,"Oh dear cd could not go back to prev directory.\n");
 		}
	else{ /* MODIFY DIRECTORY WITH ARG */
		/* STORE current_path IN temp IN CASE MODIFIED PATH DOESN'T EXIST */
 		char* temp = (char*)malloc(strlen(current_path)+1);
 		strcpy(temp,current_path);
		/* MODIFY current_path */
 		current_path = (char*)realloc(current_path, strlen(current_path)+strlen(args[1])+2);
 		strcat(current_path, "/");
 		strcat(current_path, args[1]);
		/* TRY AND CHANGE CURRENT DIRECTORY */
 		if(chdir(current_path) < 0){
			/* DIRECTORY DOES NOT EXIST, RESTORE current_path W/ TEMP */
 			free(current_path);
 			current_path = (char*)malloc(strlen(temp)+1);
 			strcpy(current_path,temp);
			/* FREE TEMP */
 			free(temp);
 			fprintf(stderr,"320sh: cd: %s: No such file or directory\n", args[1]);
 		}
		else{ /* MODIFIED DIR DOES EXIST AND HAS BEEN SET TO CURRENT DIR */
			/* STORE OLD current_path VALUE INTO prev_path */
 		free(prev_path);
 		prev_path = (char*)malloc(strlen(temp)+1);
 		strcpy(prev_path,temp);
			/* FREE TEMP */
 		free(temp);
 	}
 }
}

/*
 * echo function
 * Writes back to the screen input from the user
 */
 void echo_cmd(int arg_count, char** args){

	/* CREATE VAR FOR NUM OF SHELL ARGS TO ECHO */
 	int echo_argc = arg_count - 1;
	/* CREATE VAR TO STORE LENGTH OF ECHO STRING */
 	int echo_len = 0;
 	int i = 0;
 	int j = 0;
 	int bad_vars = 0;
	/* CREATE PTR TO FUTURE ECHO STRING */
 	char* echo_str = NULL;
	//int exists = 0;

	/* INITIALIZE DOUBLE POINTER EXPANDED_ARGS THAT WILL STORE ARGS W/ ANY EXPANDED ENV VARIABLES */
 	char** expanded_args = (char**)calloc(echo_argc, sizeof(char*));
 	char* temp = NULL;
 	for(i = 0; i < echo_argc; i++, j++) {
		/* CHECK IF ARG IS AN ENV VAR */
 		if(args[i+1][0]=='$' && args[i+1][1]!='\0'){
			/* CHECK IF THE ARG WAS $? AND IF SO RETURN RET_STATUS */
 			if(args[i+1][1]=='?' && strlen(args[i+1])==2){
 				expanded_args[j] = (char*)calloc(2, 1);
				//char c = (char)ret_status;
				//char* ptr = &c;
 				sprintf(expanded_args[j],"%d",ret_status);
				//strcpy(expanded_args[j],ptr);
 				continue;
 			}
			/* PARSE THE POSSIBLE ENV VARIABLE NAME */
 			temp = (char*)calloc(strlen(args[i+1]),1);
			/* COPY THE VARIABLE NAME W/O THE '$' */
 			strcpy(temp, args[i+1]+1);
			/* CHECK IF VAR NAME EXISTS */
 			if(getenv(temp)==NULL){
				/* ENV VAR DOESN'T EXIST */
 				fprintf(stderr,"VARIABLE DOES NOT EXIST\n");
				/* FREE TEMP AND CONSIDER THIS ARG AS NON-EXISTENT BY DECREMENTING J */
 				free(temp);
 				j--;
				/* KEEP TRACK OF BAD ARGS TO SUBTRACT FROM ECHO_ARGC LATER */
 				bad_vars++;
 				continue;
 			}
			/* ADD THE EXPANDED ENV VAR TO EXPANDED_ARGS */
 			expanded_args[j] = (char*)calloc(strlen(getenv(temp))+1, 1);
 			strcpy(expanded_args[j], getenv(temp));
			/* FREE TEMP */
 			free(temp);
 		} 
 		else{
			/* NORMAL ARG TO ECHO. ADD IT TO EXPANDED_ARGS */
 			expanded_args[j] = (char*)calloc(strlen(args[i+1])+1, 1);
 			strcpy(expanded_args[j], args[i+1]);
 		}
 	}
	/* MODIFY ECHO_ARGC TO REFLECT BAD ARGS */
 	echo_argc -= bad_vars;


	/* FIND THE ECHO STR LENGTH. INC BY 1 FOR A SPACE EACH TIME */
 	for(i = 0;i<echo_argc;i++, ++echo_len){
 		echo_len += strlen(expanded_args[i]);
 	}
	/* ALLOCATE MEMORY FOR ECHO STRING */
 	echo_str = (char*)calloc(echo_len, 1);
	/* CONCATONATE ECHO STRING */
 	for(i=0;i<echo_argc;i++){
 		strcat(echo_str, expanded_args[i]);
		/* PLACE A SPACE BETWEEN EACH WORD */
 		strcat(echo_str, " ");
 	}
	/* WRITE ECHO STRING AND NEW LINE */
 	write(1,echo_str,strlen(echo_str));
 	write(1, "\n", 1);
	/* FREE echo_str */
 	free(echo_str);
 	for(i = 0; i < echo_argc; i++) {
 		free(expanded_args[i]);
 	}
 	free(expanded_args);
 }

/*
 * Fork wrapper function
 * @return pid_t - pid of the the child to the parent. O/w 0 to the child
 * -- this is a wrapper function for the fork() function. It handles any
 *    fork error and prints to stderr accordingly
 */
 pid_t Fork(){
 	pid_t pid;
 	if((pid = fork()) < 0)
		/* FORK WAS UNSUCCESSFUL. PRINT ERROR */
 		fprintf(stderr, "%s: %s\n", "Fork Error! ", strerror(errno));
	/* FORK WAS SUCCESSFUL. RETURN PID */
 	return pid;

 }

/*
 * set_cmd function - changes the existing path with
 * the new path, otherwise creates a new name and
 * path.
 *
 * @param shell_args - a double pointer shell_args. 
 */
 void set_cmd(int shell_argc, char** shell_args) {
 	int i = 1;
 	int j = 0;
 	int flag = FALSE;
 	int nameCount = 0;
 	int pathCount = 0;
 	char *path;

 	if(shell_args[1][0] == '='){
 		fprintf(stderr, "%s\n", "This is an invalid input.");
 		return;
 	}

        /* COUNT LENGTH OF ENV NAME */
 	while(shell_args[1][j] != '\0') {
 		if(shell_args[1][j] == '=' && shell_args[1][j + 1] != '\0') {
 			flag = TRUE;
 			break;
 		} else if(shell_args[1][j] != '=') {
 			nameCount++;
 		}
 		j++;
 	}
        /* CHECK IF THERE IS A SPACE */
 	if(flag == FALSE)
 		i++;

 	if(shell_argc <= 3 && shell_argc != i + 1) {
 		fprintf(stderr, "%s\n", "This is an invalid input.");
 		return;
 	}

        /* SAVE ENV NAME */
 	if(!flag) {
 		j = 0;
 	}
 	char *name = calloc(1, nameCount);
        // char *ptr = shell_args[1];
        // while(shell_args[1][j] != '=' && shell_args[1][j] != '\0') {
 	strncat(name, shell_args[1], nameCount);

        /* CHECK FOR ANY MORE SPACES*/
 	if(flag == FALSE) {
 		if(shell_args[2][0] == '=' && shell_args[2][1] == '\0') {
 			i++;
 		}
 	}
        /* COUNT LENGTH OF PATH */
 	if(shell_argc != i + 1) {
 		fprintf(stderr, "%s\n", "This is an invalid input.");
 		return;
 	}
 	while(shell_args[i][j] != '\0') {
 		if(shell_args[i][j] != '=') {
 			pathCount++;
 		}
 		j++;
 	}
        // }
        /* SAVE ENV PATH */
 	path = calloc(pathCount,1);
 	if(!flag) {
 		j = 0;
 	}
 	if(shell_args[i][j] == '=') {
 		strncat(path, shell_args[i] + 1, pathCount);
 	} else if(flag) {
 		strncat(path, shell_args[i] + nameCount + 1, pathCount);
 	} else {
 		strncat(path, shell_args[i], pathCount);
 	}
 	if(setenv(name, path, 1)) {
 		write(1,getenv("PATH"),strlen(getenv("PATH")));
 		printf("\n");
 	}

 	free(name);
 	free(path);
 }

 void help_cmd(){
 	printf("These shell commands are defined internally. Type 'help' to see this list\n\n"
 		"cd [-] | NEW_DIR\tChanges current directory.\n\t\t\t[-] will change to the last directory."
 		"\n\t\t\tNEW_DIR will modify the current directory to NEW_DIR.\n\t\t\tIf no argument is given, the current directory\n\t\t\t"
 		"will change to the user's home directory.\n\npwd\t\t\tPrint absolute path of current location on file system.\n\n"
 		"echo STRING | ENV_VAR\tBasic echo support. Print strings and expand\n\t\t\tenvironmental variables.\n\n"
 		"set VAR_NAME = VALUE\tModify existing environmental variables and\n\t\t\tcreate new ones.\n\n"
 		"help\t\t\tPrint list of all builtin commands and\n\t\t\ttheir basic usage.\n");
 }
