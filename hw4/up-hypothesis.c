switch(*(cursor+2)) { // THE IDENTIFIER FOR WHICH ARROW KEY WAS PRESSED
	case 'A':
		/* CODE FOR ARROW UP */
		/* CHECK IF THERE IS A PREV LINE. IF NOT DO NOTHING */
		if(linec<2 || ((printLineNum-1 == currLineIndex) && linec==50) || (linec<50 && printLineNum==1)){
			continue;
		}
        if(linec==50 && printLineNum==1 && currLineIndex!=50){
            printLineNum=50;
        }
        current_line = lines[--printLineNum];
		break;
	case 'B':
	    /* CODE FOR ARROW DOWN */
		/* CHECK IF THERE IS A NEXT LINE. IF NOT DO NOTHING */
		if(!(printLineNum < currLineIndex)){
			continue;
		}
		current_line = lines[++printLineNum];
		break;
	case 'C':
		/* CODE FOR ARROW RIGHT */
		/* CHECK IF AT END OF LINE */
		break;
	case 'D':
			/* CODE FOR ARROW LEFT */
		/* CHECK IF AT BEGINNING OF LINE (CAN'T GO PAST PROMPT) */ 
		break;
}

if(linec<50){
    linec++;
    currLineIndex++;
    printLineNum = currLineIndex;
}
else{
    if(linec==50){
        currLineIndex = 1;
        printLineNum = currLineIndex;
        lines[currLineIndex-1] = current_line;
    }
}