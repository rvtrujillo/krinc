#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <poll.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netdb.h>
#include <limits.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <ifaddrs.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <openssl/sha.h>
#include <openssl/rand.h>


#define MAX_LENGTH 1024
#define EXISTING_USER 0
#define NEW_USER 1
#define INACTIVE 0
#define ACTIVE 1
#define MAX_FDS 200
#define BUFF_SIZE 1024
#define MAX_FDS  200

#define ERR_00 "ERR 00 USER NAME TAKEN"
#define ERR_01 "ERR 01 USER NOT AVAILABLE"
#define ERR_02 "ERR 02 BAD PASSWORD"
#define PROTO_TERM "\r\n\r\n"
#define CRNL "\r\n"


#define WOLFIE "WOLFIE"
#define IAM "IAM"
#define IAMNEW "IAMNEW"
#define PASS "PASS"
#define NEWPASS "NEWPASS"


#define EIFLOW "EIFLOW"
#define HINEW "HINEW"
#define AUTH "AUTH"
#define HI "HI"
#define BYE "BYE"

#define TIME "TIME"
#define LISTU "LISTU"
#define MSG "MSG"

#define EMIT "EMIT"
#define UTSIL "UTSIL"
#define CRNL "\r\n"
#define UOFF "UOFF"

#define USERS_CMD "/users\n"
#define SHUT_CMD "/shutdown\n"
#define HELP_CMD "/help\n"
#define ACCTS_CMD "/accts\n"

#define TEMP_USER "TEMP_USER"

#define ONLINE "ONLINE"
#define OFFLINE "OFFLINE"



typedef struct user{
	time_t loginT;
	int clientfd;
	char* username;
	char* password;
	char* salt;
	int active;
	struct user* next;
}User;

void Strcat2_with_space(char* buff, char* str1, char* str2);
void read_file();
void logout_protocol(int sockfd);
void create_file();
int shutdown_server();
void shutdown_handler(int sig_num);
int commands(char* command,int sockfd);
void free_args();
void printUsers();
int validate_args(int argc, char**argv);
void *login_thread(void *vargp);
int Accept(int sockfd, struct sockaddr* addr, socklen_t* addrlen);
void Setsockopt(int socket_fd, int level, int optname, const void* optval, socklen_t optlen);
void Listen(int sockfd, int backlog);
void Bind(int sockfd, const struct sockaddr *addr, socklen_t addrlen);
int invalid_args(int argc, int ocount);
const char* hash_password(char salt[], char* password);
void *comm_thread(void* vargp);
int compare(char* commands, int socket_fd);
int passCheck(char* name, int clientfd, int add_new);
void add_user(int sockfd, char* name, char* password);
int validate_password(char* pass);
char* parse_args(char* args, int* seq);
int wolfie_protocol(int clientfd);
void Strcat3(char* buff, char* str1, char* str2, char* str3);
void Strcat2(char* buff, char* str1, char* str2);
void listu_protocol(int sockfd);
char* Strcpy(char* src);
void print_prompt();

void printUsage(){
	fprintf(stdout, "./server [-h|-v] PORT_NUMBER MOTD\n"
		"-h\t\tDisplays help menu & returns EXIT_SUCCESS.\n"
		"-v\t\tVerbose print all incoming and outgoing protocol verbs & content.\n"
		"PORT_NUMBER\tPort number to listen on.\n"
		"MOTD\t\tMessage to display to the client when they connect.\n");
}

void printServerUsage(){
	fprintf(stdout, "\n/users\t\tPrint a list of currently logged in users to server's stdout.\n\n"
		"/accts\t\tPrint a list of all user accounts to server's stdout,\n\n"
		"/help\t\tShows all the commands which the server accepts and their \n\t\tfunctionality.\n\n"
		"/shutdown\tDisconnects all connected users. Saves all needed states, close\n\t\t"
		"all sockets and files and free any heap memory allocated.\n"
		);
}

