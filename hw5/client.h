#define _GNU_SOURCE 
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <poll.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netdb.h>
#include <limits.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <ifaddrs.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <sys/wait.h>
#include <signal.h> 
#include <errno.h>

#define ERR_00 "ERR 00 USER NAME TAKEN"
#define ERR_01 "ERR 01 USER NOT AVAILABLE"
#define ERR_02 "ERR 02 BAD PASSWORD"

#define ERR_00_PRINT "\x1B[1;31mERR 00 USER NAME TAKEN\x1B[0m\n"
#define ERR_01_PRINT "\x1B[1;31mERR 01 USER NOT AVAILABLE\x1B[0m\n"
#define ERR_02_PRINT "\x1B[1;31mERR 02 BAD PASSWORD\x1B[0m\n"
#define ERR_100(err) do{fprintf(stderr, "\x1B[1;31mINTERNAL SERVER ERROR: %s\x1B[0m\n",err);}while(0)
#define PROTO_TERM "\r\n\r\n"

#define WOLFIE "WOLFIE"
#define IAM "IAM"
#define IAMNEW "IAMNEW"
#define PASS "PASS"
#define NEWPASS "NEWPASS"


#define EIFLOW "EIFLOW"
#define HINEW "HINEW"
#define AUTH "AUTH"
#define HI "HI"
#define SSAP "SSAP"
#define SSAPWEN "SSAPWEN"
#define MOTD "MOTD"
#define BYE "BYE"

#define TIME "TIME"
#define LISTU "LISTU"
#define MSG "MSG"
#define UOFF "UOFF"

#define EMIT "EMIT"
#define UTSIL "UTSIL"
#define CRNL "\r\n"

#define MESSAGE 1000
#define BUFF_SIZE 1024
#define MAX_FDS 200

#define NEW_PASS_PROMPT "Enter new password> "
#define PASS_PROMPT "Enter password> "


int remove_friend(pid_t pid, char* username);
int wolfie_protocol(int serverfd);
int validate_args(int argc, char**argv);
int compare(char* commands, int socket_fd);
int Socket(int domain, int type, int protocol);
void Connect(int* sockfd, const struct sockaddr* addr, socklen_t addrlen);
void Send(int sockfd, void* buff, int len, int flags);
void Recv(int sockfd, char* buff, int flags);
void Setsockopt(int socket_fd, int level, int optname, const void* optval, socklen_t optlen);
void getClients(int sockfd);
void printClients( char* username);
int check_error(char* msg);
int recv_bye(int sockfd);
int logout_protocol();
int time_protocol();
void free_args();
int server_cmd(char* msg);
void Strcat2(char* buff, char* str1, char* str2);
int Strcmp2(char* msg, char* cmp1, char* cmp2);
int Strcmp3(char* msg, char* cmp1, char* cmp2, char* cmp3);
char* Strcpy(char* src);
int chat_protocol(char* commands, int sockfd);
int spawn_chat(char* username);
int check_friend(char* username);
char* find_friend_frm_sock(int sockfd);
void add_friend_name(char* username);
void add_friend_sock(int chatfd,char* friend_name);
void sigint_handler(int sig_num);
void Strcat5(char* buff, char* str1, char* str2, char* str3, char* str4, char* str5);
void send_msg(int chatfd, char* msg);
void print_prompt();
void chat_handler(int signum);
void sigpipe_handler(int signum);

void printUsage(){
	fprintf(stdout, "./client [-h|-c|-v] NAME SERVER_IP SERVER_PORT\n"
		"-h\t\tDisplays help menu & returns EXIT_SUCCESS.\n"
		"-c\t\tRequests to server to create a new user\n"
		"-v\t\tVerbose print all incoming and outgoing protocol verbs & content.\n"
		"NAME\tThis is the username to display when chatting.\n"
		"SERVER_IP\t\tThe ipaddress of the server to connect to.\n"
		"SERVER_PORT\t\tThe port to connect to.\n");
}

void printClientUsage(){
	fprintf(stdout, "\n/time\t\t\tAsks the server how long it has been connected appearing\n\t\t\tin hours, minutes and seconds.\n\n"
		"/help\t\t\tShows all the commands which the client accepts and their\n\t\t\tfunctionality.\n\n"
		"/logout\t\t\tDisconnects client from server.\n\n"
		"/listu\t\t\tAsks the server to show the clients that are connected\n\t\t\tto the server.\n"
		"/chat <TO> <MSG>\tBegins a chat with <TO> if they are currently logged in\n\t\t\tby sending them <MSG>.\n"
		);
}

