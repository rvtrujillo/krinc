 /* END IF (BOM TYPE CHECK) ((I THINK)) */ 
        else if((input_bom==2 && output_bom==3) || (input_bom==3 && output_bom==2)){
            printf("\nCONVERSION FROM UTF-16LE TO UTF-16BE OR THE REVERSE\n");
            if(output_bom==2)
                write_bom(output_fd, 2);
            else
                write_bom(output_fd,3);
            /* Read in UTF-16 Bytes */
            int read_val;
            int checked = 0;
            //read_value & UTF8_4_BYTE) == UTF8_4_BYTE
            while((bytes_read = read(input_fd, &read_val, 2)) == 2){
                if(!checked){
                    //printf("\nCHECKING IF HI PAIR OF SURROGATE PAIR\n");
                    /* CHECK IF POSSIBLE SURROGATE PAIR HI BYTE */
                    if((read_val > 0xD800) && (read_val < 0xDBFF)){
                        /* CHECK IF NEXT PAIR OF BYTES IS SURROGATE LOW */
                        //printf("\nCHECKING IF LOW PAIR OF SURROGATE PAIR\n");
                        if((bytes_read = read(input_fd, &read_val, 2)) == 2){
                            if((read_val > 0xDC00) && (read_val < 0xDFFF)){
                                /* THESE 4 BYTES ARE A SURRGATE PAIR! */
                                printf("SURROGATE PAIR FOUND!");
                            }
                            else{
                                //printf("\nPAIR IS NOT PART OF SURROGATE PAIR\n");
                                /* THESE 4 BYTES DO NOT MAKE UP A SURROGATE PAIR. SET FD BACK 
                                 * 4 AND READ AGAIN WITH KNOWLEDGE THAT THE FOLLOWING 2 BYTES 
                                 * ARE NOT PART OF A SURROGATE PAIR */
                                if(lseek(input_fd, -4, SEEK_CUR) < 0) {
                                    /*Unsafe action! Increment! */
                                    safe_param = *(int*) ++saftey_ptr;
                                    /* failed to move the file pointer back */
                                    perror("NULL");
                                }
                                checked = 1;
                                continue;
                            }
                        }
                     
                    }
                    else{
                        /* THESE 2 BYTES DO NOT MAKE UP A SURROGATE PAIR. SET FD BACK 
                         * 2 AND READ AGAIN WITH KNOWLEDGE THAT THE FOLLOWING 2 BYTES 
                         * ARE NOT PART OF A SURROGATE PAIR */
                        if(lseek(input_fd, -2, SEEK_CUR) < 0) {
                            /*Unsafe action! Increment! */
                            safe_param = *(int*) ++saftey_ptr;
                            /* failed to move the file pointer back */
                            perror("NULL");
                        }
                        checked = 1;
                        continue;
                    }
                }
                else{
                     /* THESE 2 BYTES ARE NOT PART OF A SURROGATE PAIR! */
                    int temp = read_val << 8;
                    //printf("LS BYTE IS: %X\n", temp);
                    read_val = read_val >> 8;
                    //printf("MS BYTE IS: %X\n", read_val);
                    read_val = read_val | temp;
                    //printf("\nBYTE PAIR IS NOT PART OF SURROGATE. VALUE IS: %X\n", read_val);
                    safe_write(output_fd, &read_val, 2);
                    checked = 0;
                }
            }
        } /* END IF (BOM TYPES CHECK) */