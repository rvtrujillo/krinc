/*
 * CSE320 HW2
 * February 20, 2016
 * @author Instructor & Robert Trujillo
 */


#include "utfconverter.h"

int main(int argc,char** argv)
{
    int opt, return_code = EXIT_FAILURE;
    int hcount = 0;
    vcount = 0;
    int ecount = 0;
    char *input_path = NULL;
    char *output_path = NULL;
    
    /* open output channel */
    FILE* standardout = fopen("stdout", "w");
    /* Parse short options */
    while((opt = getopt(argc, argv, "hve")) != -1) {
        switch(opt) {
            case 'h':
                /* The help menu was selected */
                USAGE(argv[0]);
                hcount++;
                exit(EXIT_SUCCESS);
                break;
            case 'v':
                vcount++;
                break;
            case 'e':
                ecount++;
                if(ecount>1){
                    /* A bad option was provided. */
                    USAGE(argv[0]);
                    exit(EXIT_FAILURE);
                }
                if((strcmp(argv[optind], "UTF-8"))==0){
                    output_bom = 1;
                    /* printf("\nTHIS IS THE CONVERSION ENCODING: %s\n", argv[optind]); */
                    optind++;
                }
                else if((strcmp(argv[optind], "UTF-16LE"))==0){
                    output_bom = 2;
                    /* printf("\nTHIS IS THE CONVERSION ENCODING: %s\n", argv[optind]); */
                    optind++;
                }
                else if((strcmp(argv[optind], "UTF-16BE"))==0){
                    output_bom = 3;
                    /* printf("\nTHIS IS THE CONVERSION ENCODING: %s\n", argv[optind]); */
                    optind++;
                }
                else{
                    /* A bad option was provided. */
                    fprintf(stderr, "The flag '-e' has an incorrect value '%s'.\n", argv[optind]);
                    /* printf("\n-e HAS AN INCORRECT VALUE EXIT FAILURE\n"); */
                    USAGE(argv[0]);
                    exit(EXIT_FAILURE);
                }
                break;           
            case '?':
                /* Let this case fall down to default;
                 * handled during bad option.
                 */
            default:
                /* A bad option was provided. */
                USAGE(argv[0]);
                /* printf("\nDEFAULT EXIT FAILURE\n"); */
                exit(EXIT_FAILURE);
                break;
        }
    }
    /* printf("\nTHE NUM OF -V ARGS IS: %d\n ", vcount);*/
    /* Get position arguments */
    if(optind < argc && (argc - optind) == 2) {
        input_path = argv[optind++];
        output_path = argv[optind++];
    } else {
        if((argc - optind) <= 0) {
            fprintf(stderr, "Missing INPUT_FILE and OUTPUT_FILE.\n");
        } else if((argc - optind) == 1) {
            fprintf(stderr, "Missing OUTPUT_FILE.\n");
        } else {
            fprintf(stderr, "Too many arguments provided.\n");
        }
        USAGE(0[argv]);
        exit(EXIT_FAILURE);
    }
    /* Make sure all the arguments were provided */
    if(input_path != NULL || output_path != NULL) {
        int input_fd = -1, output_fd = -1;
        bool success = false;
        switch(validate_args(input_path, output_path)) {
                case VALID_ARGS:
                    /* Attempt to open the input file */
                    if((input_fd = open(input_path, O_RDONLY)) < 0) {
                        fprintf(stderr, "Failed to open the file %s\n", input_path);
                        perror(NULL);
                        goto conversion_done;
                    }
                    /* Delete the output file if it exists. Don't care about return code. */
                    unlink(output_path);
                    /* Attempt to create the file */
                    if((output_fd = open(output_path, O_CREAT | O_WRONLY, /*CHANGED FROM != */
                        S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP)) < 0) {
                        /* Tell the user that the file failed to be created */
                        fprintf(stderr, "Failed to open the file %s\n", input_path);
                        perror(NULL);
                        goto conversion_done;
                    }
                    /* Start the conversion */

                    success = convert(input_fd, output_fd, vcount, standardout);
conversion_done:
                    if(success) {
                        /* We got here so it must of worked right? */
                        return_code = EXIT_SUCCESS;
                        printf("The file %s was successfully created.\n", output_path);
                        close(input_fd);
                        close(output_fd);
                    } else {
                        /* Conversion failed; clean up */
                        printf("The file %s was not created.\n", output_path);
                        if(output_fd < 0 && input_fd >= 0) {
                            close(input_fd);
                        }
                        if(output_fd >= 0) {
                            close(output_fd);
                            unlink(output_path);
                        }
                        /* Just being pedantic... */
                        return_code = EXIT_FAILURE;
                    }
                    break;
                case SAME_FILE:
                    fprintf(stderr, "The output file %s was not created. Same as input file.\n", output_path);
                    break;
                case FILE_DNE:
                    fprintf(stderr, "The input file %s does not exist.\n", input_path);
                    break;
                case BAD_BOM:
                    fprintf(stderr, "The input file %s has an improper Byte Order Mark.\n", input_path);
                    break;
                default:
                    fprintf(stderr, "An unknown error occurred\n");
                    
        }
    } else {
        /* Alert the user what was not set before quitting. */
        if((input_path = NULL) == NULL) {
            fprintf(stderr, "INPUT_FILE was not set.\n");
        }
        if((output_path = NULL) == NULL) {
            fprintf(stderr, "OUTPUT_FILE was not set.\n");
        }
        /* Print out the program usage*/
        USAGE(argv[0]);
    }
    fclose(standardout);
    fclose(stdout);
    fclose(stdin);
    fclose(stderr);
    return return_code;
}

int validate_args(const char* input_path, const char* output_path)
{
    int return_code = FAILED;
    /* number of arguments */
    int vargs = 2;
    /* create reference */
    void* pvargs = &vargs;
    /* Make sure both strings are not NULL */
    if(input_path != NULL && output_path != NULL) {
        /* Check to see if the the input and output are two different files. */
        if(strcmp(input_path, output_path) != 0) {

            struct stat in;
            struct stat out;
            
            stat(input_path, &in);
            stat(output_path, &out);
            
            if((in.st_dev == out.st_dev) && (in.st_ino == out.st_ino)){
                return_code = SAME_FILE;
                printf("\nTHE SYSTEM SAME FILE CHECK WORKED BETTER\n");
            }

            /* Check to see if the input file exists */
            struct stat sb;
            /* zero out the memory of one sb plus another */
            memset(&sb, 0, sizeof(sb) + 1);
            /* increment to second argument */
            pvargs++;
            /* now check to see if the file exists */
            if(stat(input_path, &sb) == -1) {
                /* something went wrong */
                if(errno == ENOENT) {
                    /* File does not exist. */
                    return_code = FILE_DNE;
                } else {
                    /* No idea what the error is. */
                    perror("NULL");
                }

            } else {
                int input_fd = -1, output_fd = -1;
                auto int safe_param = SAFE_PARAM;/* DO NOT DELETE, PROGRAM WILL BE UNSAFE */
                void* saftey_ptr = &safe_param;
                auto ssize_t bytes_read;
                unsigned char bom[3];
                if((input_fd = open(input_path, O_RDONLY)) != 0) {
                    if((output_fd = open(output_path, O_CREAT | O_WRONLY, /*CHANGED FROM != */
                        S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP)) != 0){
                            if((bytes_read = read(input_fd, bom, 3))==3){
                            /* CHECK FIRST 3 BYTES FOR BOM*/
                            if(bom[0]==0xEF && bom[1]==0xBB && bom[2]==0xBF)
                                input_bom = 1; /* INPUT FILE IS ENCODED IN UTF-8 */
                            else{ /* WASN'T UTF-8 SO SET FD BACK 1 BYTE */
                                /* Set the file position back 1 byte */
                                if(lseek(input_fd, -1, SEEK_CUR) < 0) {
                                    /*Unsafe action! Increment! */
                                    safe_param = *(int*)++saftey_ptr;
                                    /* failed to move the file pointer back */
                                    perror("NULL");
                                }
                                if(bom[0]==0xFF && bom[1]==0xFE)
                                    input_bom = 2; /* INPUT FILE IS ENCODED IN UTF-16LE */
                                else if (bom[0]==0xFE && bom[1]==0xFF) /* INPUT FILE IS ENCODED IN UTF-16BE */
                                    input_bom = 3;
                                else {
                                    input_bom = 0;
                                }
                            }
                        } /* END IF */
                    }
                }


                
                #ifdef CSE320
                    char host[32];
                    gethostname(host, sizeof(host));
                    char* encodes[3] = {"UTF-8", "UTF-16LE", "UTF-16BE"};
                    int in_index;
                    if(input_bom==1)
                        in_index = 0;
                    else if(input_bom==2)
                        in_index = 1;
                    else 
                        in_index = 2;
                    cse320(host, input_path, sb.st_ino, sb.st_dev, sb.st_size,output_path, encodes[in_index]);
                    output_encode(encodes[output_bom-1]);
                #endif

                
                return_code = VALID_ARGS;
            }
        }
        else{
            struct stat in;
            struct stat out;
            
            stat(input_path, &in);
            stat(output_path, &out);
            
            if((in.st_dev == out.st_dev) && (in.st_ino == out.st_ino)){
                return_code = SAME_FILE;
                printf("\nTHE SYSTEM SAME FILE CHECK WORKED\n");
            }
            
        } 
    }
    
    return return_code;
}

bool convert(const int input_fd, const int output_fd, int vs, FILE* standardout)
{
    char str_base[128];

    if(vcount > 0){
        
        if(vcount == 1){
            sprintf(str_base, "+---------+------------+------------------------+\n");
            fprintf(stderr, "%s|  ASCII  | # of bytes |\tcodepoint\t|\n%s", str_base, str_base);
        }
        else if(vcount == 2){
            sprintf(str_base, "+---------+------------+------------------------+--------------+\n");
            fprintf(stderr, "%s|  ASCII  | # of bytes |\tcodepoint\t|     input    |\n%s", str_base, str_base);
        }
        else{
            sprintf(str_base, "+---------+------------+------------------------+--------------+--------------+\n");
            fprintf(stderr, "%s|  ASCII  | # of bytes |\tcodepoint\t|     input    |    output    |\n%s", str_base, str_base);
        }
    }
    char ascii;
    int numBytes;
    int codepoint;
    int bytes_countdown=1;
    int input;
    int output;
    int value =0;
    
    /* IF V-FLAGS START PREP FOR PRINTING VERBOSE TO STDOUT */
    bool success = false;
    if(input_fd >= 0 && output_fd >= 0) {
        /* UTF-8 encoded text can be @ most 4-bytes */
        unsigned char bytes[4];
        auto unsigned char read_value;
        auto size_t count = 0;
        auto int safe_param = SAFE_PARAM;/* DO NOT DELETE, PROGRAM WILL BE UNSAFE */
        void* saftey_ptr = &safe_param;
        auto ssize_t bytes_read;
        bool encode = false;
        unsigned char bom[3];

        if((bytes_read = read(input_fd, bom, 3))==3){
                            /* CHECK FIRST 3 BYTES FOR BOM*/
                            if(bom[0]==0xEF && bom[1]==0xBB && bom[2]==0xBF)
                                input_bom = 1; /* INPUT FILE IS ENCODED IN UTF-8 */
                            else{ /* WASN'T UTF-8 SO SET FD BACK 1 BYTE */
                                /* Set the file position back 1 byte */
                                if(lseek(input_fd, -1, SEEK_CUR) < 0) {
                                    /*Unsafe action! Increment! */
                                    safe_param = *(int*)++saftey_ptr;
                                    /* failed to move the file pointer back */
                                    perror("NULL");
                                }
                                if(bom[0]==0xFF && bom[1]==0xFE)
                                    input_bom = 2; /* INPUT FILE IS ENCODED IN UTF-16LE */
                                else if (bom[0]==0xFE && bom[1]==0xFF) /* INPUT FILE IS ENCODED IN UTF-16BE */
                                    input_bom = 3;
                                else {
                                    input_bom = 0;
                                }
                            }
                        } /* END IF */
        /* WRITE THE CORRECT BOM TO THE FILE */
        if(output_bom==2){
            write_bom(output_fd, 2);
            /* printf("\nCONVERSION TO UTF-16LE\n"); */
        }
        else if(output_bom==3){
            write_bom(output_fd, 3);
            /* printf("\nCONVERSION TO UTF-16BE\n"); */
        }
        else{
            write_bom(output_fd, 1);
            /* printf("\nCONVERSION TO UTF-8\n");*/
        }    
        

        /* CHECK FOR CONVERSION FROM UTF-8 TO UTF-16LE OR 16BE */
        if(input_bom == 1)
        {
            int write = 1; /* VARIABLE TO KEEP TRACK OF A BACKSTEPPED FD */
            /* Read in UTF-8 Bytes */
            while((bytes_read = read(input_fd, &read_value, 1)) == 1){
                if(output_bom == 1){ /* CONVERTING FROM UTF-8 TO UTF-8 */

                    if(write){
                        safe_write(output_fd, &read_value, 1);
                        /* printf("THIS IS BEING WRITTEN TO OUTPUT.TXT: %X\n", read_value); */
                    }
                    else
                        write = 1;
                }
                
                ascii = -1;
                
                /* Mask the most significant bit of the byte */
                unsigned char masked_value = read_value & 0x80;
                if(masked_value == 0x80) {
                    if((read_value & UTF8_4_BYTE) == UTF8_4_BYTE ||
                        (read_value & UTF8_3_BYTE) == UTF8_3_BYTE ||
                        (read_value & UTF8_2_BYTE) == UTF8_2_BYTE) {
                            /* Check to see which byte we have encountered*/
                            if(count == 000) {
                                count++[bytes] = read_value;
                            }
                            else {
                                write = 0;
                                /* Set the file position back 1 byte */
                                if(lseek(input_fd, -1, SEEK_CUR) < 0){
                                  
                                    /*Unsafe action! Increment! */
                                    safe_param = *(int*)++saftey_ptr;
                                    /* failed to move the file pointer back */
                                    perror("NULL");
                                    goto conversion_done;
                                }
                                /* Encode the current values into UTF-16LE */
                                encode = true;
                            }
                    }
                    else if((read_value & UTF8_CONT) == UTF8_CONT) {
                        /* continuation byte */
                        bytes[count++] = read_value;
                        }
                } /* END IF */
                else {
                    if(count == 000) {
                        /* US-ASCII */
                        bytes[count++] = read_value;
                        encode = true;
                    }
                    else {
                        /* Found an ASCII character but theres other characters
                        * in the buffer already.
                        * Set the file position back 1 byte.
                        */
                        write = 0;
                        if(lseek(input_fd, -1, SEEK_CUR) < 0) {
                            /*Unsafe action! Increment! */
                            safe_param = *(int*) ++saftey_ptr;
                            /* failed to move the file pointer back */
                            perror("NULL");
                            goto conversion_done;
                        }
                        /* Encode the current values into UTF-16LE */
                        encode = true;
                    }
                }
                /* If its time to encode do it here */
                if(encode) {
                    int i;
                    int temp;
                    value = 0;
                    bool isAscii = false;
                    for(i=0; i < count; i++) {
                        if(i == 0) {
                            if((bytes[i] & UTF8_4_BYTE) == UTF8_4_BYTE) {
                                value = bytes[i] & 0x7;
                                bytes_countdown = 4;
                                temp = bytes[i] << 24;
                                input = bytes[i+1] << 16;
                                input = input | temp;
                                temp = bytes[i+2] << 8;
                                input = input | temp;
                                temp = bytes[i+3];
                                input = input | temp;
                                numBytes = 4;
                            }
                            else if((bytes[i] & UTF8_3_BYTE) == UTF8_3_BYTE) {
                                value =  bytes[i] & 0xF;
                                bytes_countdown = 3;
                                
                                temp = bytes[i] << 16;
                                input = bytes[i+1] << 8;
                                input = input | temp;
                                temp = bytes[i+2];
                                input = input | temp;
                                
                                numBytes = 3;
                            }
                            else if((bytes[i] & UTF8_2_BYTE) == UTF8_2_BYTE) {
                                value =  bytes[i] & 0x1F;
                                bytes_countdown = 2;
                                
                                temp = bytes[i] << 8;
                                input = bytes[i+1];
                                input = input | temp;
                                
                                numBytes = 2;
                            }
                            else if((bytes[i] & 0x80) == 0) {
                                /* Value is an ASCII character */
                                numBytes = 1;
                                bytes_countdown = 1;
                                input = bytes[i];
                                value = bytes[i];
                                ascii = bytes[i];
                                isAscii = true;
                            }
                            else {
                                /* Marker byte is incorrect */
                                goto conversion_done;
                            }
                        }
                        else {
                            if(!isAscii) {
                                value = (value << 6) | (bytes[i] & 0x3F);
                                bytes_countdown--;
                            }
                            else {
                                /* How is there more bytes if we have an ascii char? */
                                goto conversion_done;
                            }
                        }
                    }
                    codepoint = value;
                    output = value;
                    /* Handle the value if its a surrogate pair*/
                    if(value >= SURROGATE_PAIR) {
                        int vprime = value - SURROGATE_PAIR;
                        int w1 = (vprime >> 10) + 0xD800;
                        int w2 = (vprime & 0x3FF) + 0xDC00;
                        /* write the surrogate pair to file */
                        if(output_bom == 3){ /* CHECK IF NEED TO ENCODE IN BE */
                            int temp;
                            temp = w1 >> 8;
                            w1 = w1 << 8;
                            w1 = w1 | temp;
                            temp = w2 >> 8;
                            w2 = w2 << 8;
                            w2 = w2 | temp;  
                        }
                        if(output_bom > 1){
                            safe_write(output_fd, &w1, CODE_UNIT_SIZE);  
                            safe_write(output_fd, &w2, CODE_UNIT_SIZE);   
                        }               
                    }
                    else {
                        /* write the code point to file */
                        if(output_bom==3){ /* CHECK IF NEED TO ENCODE IN BE */
                            int temp;
                            temp = value >> 8;
                            value = value << 8;
                            value = value | temp;
                            value = value & 0x00ffff;
                        }
                        if(output_bom > 1) 
                            safe_write(output_fd, &value, CODE_UNIT_SIZE);
                    
                    }
                    if(output_bom == 1){
                        --bytes_countdown;
                        printVerbose(ascii, numBytes, codepoint, input, input, standardout, str_base);
                    }
                    /* Done encoding the value to UTF-16LE */
                    encode = false;
                    count = 0;
                }
                
                if((vcount > 0) && ((--bytes_countdown)==0) && (ascii<0 || ((ascii >=32) && (ascii<=127))))
                    printVerbose(ascii, numBytes, codepoint, input, output, standardout, str_base);
            }
        } /* END IF (BOM TYPE CHECK) */
        /* CONVERT FROM UTF16-LE TO BE OR BE TO LE. JUST SWAP BYTES */
        else if((input_bom==2 && output_bom==3) || (input_bom==3 && output_bom==2)){
            int temp;
            while((bytes_read = read(input_fd, &read_value, 1)) == 1){
                    temp = read_value;
                    read(input_fd, &read_value, 1);
                    safe_write(output_fd, &read_value, 1);
                    safe_write(output_fd, &temp, 1);
            }
        }
        else if((input_bom == output_bom) && (input_bom > 1)){ /* CONVERTING SAME TYPE OF UTF-16 */
            while((bytes_read = read(input_fd, &read_value, 1)) == 1){
                safe_write(output_fd, &read_value, 1);
            }
        }
    } 
        
    /* If we got here the operation was a success! */
    success = true;     
    

conversion_done:
    
    return success;
}

bool safe_write(int output_fd, void* value, size_t size)
{
    bool success = true;
    ssize_t bytes_written;
    if((bytes_written = write(output_fd, value, size)) != size) {
        /* The write operation failed */
        fprintf(stdout, "Write to file failed. Expected %zu bytes but got %zd\n", size, bytes_written);
    }
    return success;
}

void write_bom(int output_fd, int bom_type){
    int bom_value;
    if(bom_type==1){ /* WRITE UTF-8 BOM */
        bom_value = 0xBFBBEF;
        safe_write(output_fd, &bom_value , 3);
        /* printf("\nWRITE UTF-8 BOM\n"); */
    }
    else if(bom_type == 2){ /* WRITE UTF-16LE BOM */
        bom_value = 0xFEFF;
        safe_write(output_fd, &bom_value , 2);
        /* printf("\nWRITE UTF-16LE BOM\n"); */
    }
    else if(bom_type == 3){ /* WRITE UTF-16BE BOM */
        bom_value = 0xFFFE;
        safe_write(output_fd, &bom_value , 2);
        /* printf("\nWRITE UTF-16BE BOM\n"); */
    }
}

void printVerbose(char ascii, int numBytes, int codepoint, int input, int output, FILE* standardout, char str_base[128]){
    /* IF V-FLAGS START PREP FOR PRINTING VERBOSE TO STDOUT */
        int temp = 0;
        if(((input_bom == 1) || (input_bom == 3)) && (output_bom == 2))
        {
            temp = output << 8;
            output = output >> 8;
            temp = output | temp;
            temp = temp & 0x00ffff;
            output = temp;
        }
        if(vcount == 1){
            if(ascii >= 32 && ascii <= 127){
                if(output_bom==2)
                    fprintf(stderr, "|    %c    |     %d      |\tU+00%X\t\t|\n%s", ascii, numBytes,codepoint, str_base);
                else
                    fprintf(stderr, "|    %c    |     %d      |\tU+00%X\t\t|\n%s", ascii, numBytes,codepoint, str_base);
            }
            else if(numBytes<=2){
                if(output_bom==2)
                    fprintf(stderr, "|  NONE   |     %d      |\tU+00%X\t\t|\n%s", numBytes,codepoint, str_base);
                else
                    fprintf(stderr, "|  NONE   |     %d      |\tU+00%X\t\t|\n%s", numBytes,codepoint, str_base);
            }
            else if (numBytes==3 || numBytes==4)
                fprintf(stderr, "|  NONE   |     %d      |\tU+%X\t\t|\n%s", numBytes,codepoint, str_base);
        }
        else if(vcount == 2){
            if(ascii >= 32 && ascii <= 127){
                if(output_bom==2)
                    fprintf(stderr, "|    %c    |     %d      |\tU+00%X\t\t|  0x%X        |\n%s", ascii, numBytes,codepoint, input, str_base);
                else
                    fprintf(stderr, "|    %c    |     %d      |\tU+00%X\t\t|  0x%X        |\n%s", ascii, numBytes,codepoint, input, str_base);
            }
            else if(numBytes==2)
                fprintf(stderr, "|  NONE   |     %d      |\tU+00%X\t\t|  0x%X      |\n%s", numBytes,codepoint, input, str_base);
            else if(numBytes==3)
                fprintf(stderr, "|  NONE   |     %d      |\tU+%X\t\t|  0x%X    |\n%s", numBytes,codepoint, input, str_base);
            else if (numBytes==4)
                fprintf(stderr, "|  NONE   |     %d      |\tU+%X\t\t|  0x%X  |\n%s", numBytes,codepoint, input, str_base);

        }
        else{
            if(ascii >= 32 && ascii <= 127){
                if(output_bom==2)
                    fprintf(stderr, "|    %c    |     %d      |\tU+00%X\t\t|  0x%X        |  0x%X      |\n%s", ascii, numBytes,codepoint, input, output, str_base);
                else if(output_bom ==3)
                    fprintf(stderr, "|    %c    |     %d      |\tU+00%X\t\t|  0x%X        |  0x00%X      |\n%s", ascii, numBytes,codepoint, input, output, str_base);
                else
                    fprintf(stderr, "|    %c    |     %d      |\tU+00%X\t\t|  0x%X        |  0x%X      |\n%s", ascii, numBytes,codepoint, input, output, str_base);
            }
            else if(numBytes==2){
                if(output_bom==2)
                    fprintf(stderr, "|  NONE   |     %d      |\tU+00%X\t\t|  0x%X      |  0x%X       |\n%s", numBytes,codepoint, input, output, str_base);
                else
                    fprintf(stderr, "|  NONE   |     %d      |\tU+00%X\t\t|  0x%X      |  0x00%X     |\n%s", numBytes,codepoint, input, output, str_base);
            }
            else if(numBytes==3){
                fprintf(stderr, "|  NONE   |     %d      |\tU+%X\t\t|  0x%X    |  0x%X      |\n%s", numBytes,codepoint, input, output, str_base);
            }
            else if (numBytes==4)
                fprintf(stderr, "|  NONE   |     %d      |\tU+%X\t\t|  0x%X  |  0x%X      |\n%s", numBytes,codepoint, input, output, str_base);
        }
    }


