Robert Trujillo
ID# - 110668776
HW 2
2/19/2016

USING A GRACE DAY

1. The program does not print out a proper verbose statement for any
   conversions where the input is encoded with UTF-16LE or UTF-16BE.

2. The program cannot convert files from UTF-16LE to UTF-8.

3. The program cannot convert files from UTF-16BE to UTF-8.

4. The program cannot run on sparky.

5. Due to confusion of instructions, verbose prints soley to stdout.
