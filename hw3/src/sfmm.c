/**
 * All functions you make for the assignment must be implemented in this file.
 * Do not submit your assignment with a main function in this file.
 * If you submit with a main function in this file, you will get a zero.
 */
#include <stdio.h>
#include <stdlib.h>
#include "sfmm.h"
#include <errno.h>
#include <string.h>

/**
 * You should store the head of your free list in this variable.
 * Doing so will make it accessible via the extern statement in sfmm.h
 * which will allow you to pass the address to sf_snapshot in a different file.
 */
sf_free_header* freelist_head = NULL;
sf_footer *footer = NULL;
sf_free_header* tempFreeHead = NULL;
sf_free_header* allocatedBlk = NULL;


#define PAGE_SIZE 4096

size_t align(size_t reqBytes);
void coalesce(sf_free_header *bp);
sf_free_header* findFit(size_t size);
void updateHeaderFooter(size_t oldSize, size_t payLoadSize, sf_free_header* allocated);
size_t heapSize = 0;
//int brk = 0;

/*
 * sf_malloc - Will take an arg specifying the amount of space to allocate
 * arg size  - the specified size to allocate
 * returns a void ptr to the newly allocated space
 */
void* sf_malloc(size_t size) {
	int numFreeFlags = 0;
	int numPlaceFlags = 0;
	#ifdef ADDRESS
		numFreeFlags++;
	#endif
	#ifdef LIFO
		numFreeFlags++;
	#endif
	#ifdef FIRST
		numPlaceFlags++;
	#endif
	#ifdef NEXT
		numPlaceFlags++;
	#endif

	if(numFreeFlags>1 || numPlaceFlags>1)
	{
		errno = E2BIG;
		printf("\n\nSFJDFJDFJSD");
		return NULL;
	}



	if(size > (unsigned long long)1024*1024*1024*4 || size<=0)
	{
		errno = EINVAL;
		return NULL;
	}
	/* HEAP SIZE CANNOT SURPASS 20MB */
	if(heapSize>(20 * (1 << 20))){
		errno = ENOMEM;
		return NULL;
	}
	if(freelist_head == NULL){
		//int padding = (unsigned long)(sf_sbrk(0)) % 16;
		//if(padding == 0)
		/* MAKE THE PADDING FOR THE ALIGNMENT A FOOTER TO SHOW THAT IT IS ALLOCATED (PERMANENTLY BWAHA) */
		sf_footer* padFoot = sf_sbrk(8);
		heapSize += 8;
		padFoot->alloc = 1;

		
		//else sf_sbrk(8-padding);

		/* IF FREELIST_HEAD IS NULL HAVE IT POINT TO FIRST AVAILABLE MEMORY IN HEAP + 8 (FOR ALIGNMENT) */
		freelist_head = (sf_free_header*)sf_sbrk(PAGE_SIZE);
		/* INC HEAP SIZE VARIABLE */
		heapSize+=PAGE_SIZE;

		/* ADD PADDING OF 8 BYTES SO ALL PAYLOADS ARE LOCATED ON AN ADDRESS DIVISIBLE BY 16 */
		//freelist_head =  (sf_free_header*)(((char*)freelist_head));
		
		/* THE FIRST NEW FREE BLOCK WILL POINT TO ITSELF WITH NEXT & PREV */
		freelist_head->next = NULL; 
		freelist_head->prev = NULL;
		
		/* EDIT THE ALLOC FLAG FOR "FREE" AND EDIT THE SIZE OF ENTIRE BLOCK (FIRST BLOCK SO PAGE_SIZE) */
		freelist_head->header.alloc = 0;
		freelist_head->header.block_size = PAGE_SIZE >> 4;
		
		/* PLACE FOOTER JUST BEFORE END OF HEAP */
		footer = (sf_footer*)(((char*)sf_sbrk(0))-SF_FOOTER_SIZE);
		footer->block_size = PAGE_SIZE;
		footer->alloc = 0;
	}
	
	/* ALIGN THE PAYLOAD */
	size_t payLoadSize = align(size);
	
	/* CHECK IF FLAG SIGNALS FIRST-FIT OR NEXT-FIT */
	#ifdef NEXT
		/* NEXT-FIT WAS FLAGGED BY PRE-PROCESSOR AND CURRENT PTR HAS NO VALUE YET */
		if(tempFreeHead==NULL || tempFreeHead->next == NULL)
			/* START AT BEGINNING THEN */
			tempFreeHead = freelist_head;
	#else
		/* FIRST-FIT WAS FLAGGED BY PRE-PROCESSOR OR BY DEFAULT */
		tempFreeHead = freelist_head;
	#endif

	/* FIND PTR TO FREE BLOCK WITH ENOUGH SPACE */
	tempFreeHead = findFit(payLoadSize);

	if(tempFreeHead == NULL){
		return NULL;
		sf_free_header* newPage = sf_sbrk(PAGE_SIZE);
		/* EDIT THE ALLOC FLAG FOR "FREE" AND EDIT THE SIZE OF ENTIRE BLOCK (FIRST BLOCK SO PAGE_SIZE) */
		newPage->header.alloc = 0;
		newPage->header.block_size = PAGE_SIZE >> 4;
		sf_free_header* temp = freelist_head;
		while(temp->next != NULL){
			temp=temp->next;
		}
		temp->next = newPage;
		newPage->prev = temp;
		newPage->next = NULL;
		
		
		/* PLACE FOOTER JUST BEFORE END OF HEAP (END OF FIRST FREE BLOCK) */
		footer = (sf_footer*)(((char*)sf_sbrk(0))-SF_FOOTER_SIZE);
		footer->block_size = PAGE_SIZE>>4;
		footer->alloc = 0;
		sf_blockprint(newPage);
		coalesce(newPage);
		heapSize += PAGE_SIZE;
		sf_malloc(size);
	}
	else{
		/* REMEMBER THE BLOCK SIZE OF FREE BLOCK THAT IS BEING SPLIT */
		size_t oldBlockSize = tempFreeHead->header.block_size;

		/* CHANGE REQUESTED & BLOCK SIZE OF ALLOCATED BLOCK HEADER */
		tempFreeHead->header.requested_size = size;
		tempFreeHead->header.block_size = SF_HEADER_SIZE + payLoadSize + SF_FOOTER_SIZE;
		tempFreeHead->header.block_size = (tempFreeHead->header.block_size) >> 4;

		/* SWITCH ALLOCATED FLAG TO "ALLOCATED" */
		tempFreeHead->header.alloc = 1;

		/* CREATE PTR TO THE ALLOCATED BLK HEADER FOR WHEN tempFreeHead CHANGES */
		allocatedBlk = tempFreeHead;

		/* CHANGE PTR TO FREE BLOCK AND EDIT THE NEXT & PREV */
		/* IF tempFreeHead IS THE FIRST FREE BLOCK, UPDATE freelist_head PTR TO NEXT BLOCK */
		if(tempFreeHead == freelist_head && freelist_head->next == NULL){
			freelist_head =(sf_free_header*)(((char*)freelist_head) + (tempFreeHead->header.block_size << 4));
			freelist_head->prev = NULL;
			freelist_head->next = NULL;
			tempFreeHead = freelist_head;
			
			/* PLACE NEW FOOTER FOR ALLOCATED BLOCK RIGHT BEFORE HEADER OF FREE BLOCK */
			footer = (sf_footer*)(((char*)allocatedBlk) + SF_HEADER_SIZE + payLoadSize);
			footer->block_size = SF_HEADER_SIZE + payLoadSize + SF_FOOTER_SIZE;
			footer->block_size = (footer->block_size) >> 4;
			footer->alloc = 1;

			/* SPECIAL CASE WHERE FREE BLK WILL ALWAYS SPLIT SINCE THERE'S ONLY ONE FREE BLK */
			tempFreeHead->header.block_size = oldBlockSize - footer->block_size;
			tempFreeHead->header.alloc = 0;
			footer = (sf_footer*)(((char*)tempFreeHead) + (tempFreeHead->header.block_size<<4) - SF_FOOTER_SIZE);
			footer->block_size = tempFreeHead->header.block_size;
			footer->alloc = tempFreeHead->header.alloc;
		}
		/* FIRST FREE BLK WILL BE ALLOCATED BUT FREELIST HAS MORE THAN 1 FREE BLK*/
		else if(tempFreeHead == freelist_head && freelist_head->next != NULL){
			/* ENTIRE BLOCK WILL BE ALLOCATED */
			if(oldBlockSize == freelist_head->header.block_size){
				freelist_head = freelist_head->next;
				freelist_head->prev = NULL;
				tempFreeHead = freelist_head;
			}
			/* FREE BLK WILL BE SPLIT */
			else{
				freelist_head = (sf_free_header*)(((char*)freelist_head) + (tempFreeHead->header.block_size << 4));
				freelist_head->next = tempFreeHead->next;
				freelist_head->prev = NULL;
				freelist_head->next->prev = freelist_head;
				tempFreeHead = freelist_head;
				//updateHeaderFooter(oldBlockSize, payLoadSize, allocatedBlk);
			}
		}
		/* LAST BLK IN FREELIST WITH MULTIPLE FREE BLKS WILL BE ALLOCATED */
		else if(tempFreeHead->next == NULL){
			/* ENTIRE BLOCK WILL BE ALLOCATED */
			if(oldBlockSize == tempFreeHead->header.block_size){
				tempFreeHead->prev->next = NULL;
				/* PUT tempFreeHead BACK TO BEGINNING */
				tempFreeHead = freelist_head;
			}
			/* FREE BLK WILL BE SPLIT */
			else{
				tempFreeHead->prev->next = (sf_free_header*)(((char*)tempFreeHead) + (tempFreeHead->header.block_size << 4));
				tempFreeHead->prev->next->prev = tempFreeHead->prev;
				tempFreeHead = (sf_free_header*)(((char*)tempFreeHead) + (tempFreeHead->header.block_size << 4));
				tempFreeHead->next = NULL;
				//updateHeaderFooter(oldBlockSize, payLoadSize, allocatedBlk);
			}
		}
		else if(tempFreeHead->next != NULL){
			/* ENTIRE BLOCK WILL BE ALLOCATED */
			if(oldBlockSize == tempFreeHead->header.block_size){
				tempFreeHead->prev->next = tempFreeHead->next;
				tempFreeHead->next->prev = tempFreeHead->prev;
				tempFreeHead = tempFreeHead->next;
			}
			/* FREE BLK WILL BE SPLIT */
			else{
				tempFreeHead->prev->next = (sf_free_header*)(((char*)tempFreeHead) + (tempFreeHead->header.block_size << 4));
				tempFreeHead->prev->next->prev = tempFreeHead->prev;
				tempFreeHead->prev->next->next = tempFreeHead->next;
				tempFreeHead = (sf_free_header*)(((char*)tempFreeHead) + (tempFreeHead->header.block_size << 4));
				tempFreeHead->next->prev = tempFreeHead;
				//updateHeaderFooter(oldBlockSize, payLoadSize, allocatedBlk);
			}
		}

		/* IMPORTANT - AFTER THIS LINE tempFreeHead WILL POINT TO THE NEXT FREE BLOCK */ 
		//tempFreeHead = (sf_free_header*)(((char*)tempFreeHead) + (tempFreeHead->header.block_size << 4));
		
		/* PLACE NEW FOOTER FOR ALLOCATED BLOCK RIGHT BEFORE HEADER OF FREE BLOCK */
		footer = (sf_footer*)(((char*)allocatedBlk) + SF_HEADER_SIZE + payLoadSize);
		footer->block_size = SF_HEADER_SIZE + payLoadSize + SF_FOOTER_SIZE;
		footer->block_size = (footer->block_size) >> 4;
		footer->alloc = 1;

		/* IF THE FREE BLK WAS SPLIT UPDATE THE HEADER AND FOOTER*/
		if(oldBlockSize != allocatedBlk->header.block_size)
			updateHeaderFooter(oldBlockSize, payLoadSize, allocatedBlk);

		/* GET THE ALLOCATED BLOCK FOOTER TO HAVE ACCESS TO THE SIZE FOR WHEN WE RETURN */
		//footer = (sf_footer*)(((char*)tempFreeHead)-SF_FOOTER_SIZE);

	}
	
	//printf("The size requested was %lu and the aligned size is %lu\n", size, payLoadSize);
	/* 
	 * RETURN THE PTR TO THE PAYLOAD OF THE PREVIOUS BLOCK. SO...
	 * TAKE THE HEADER OF THE FREE BLOCK, SUBTRACT THE ALLOCATED BLOCK SIZE TO GET TO THE PREV HEADER,
	 * THEN ADD SF_HEADER_SIZE TO GET TO BEGINNING OF THE PAYLOAD. BOOM. YOUR WELCOME.
	 */
	 //sf_blockprint(freelist_head);
    return ((void*)(((char*)allocatedBlk) + SF_HEADER_SIZE));
}

/*
 * sf_free - takes a ptr to allocated space and changes the allocated flag/bit to 0.
 *           Will also coalesce if previous/next block is also a free block to prevent
 *           false fragmentation.
 * arg ptr - A ptr to the allocated block to free
 * returns nothing
 */
void sf_free(void *ptr) {

	/* SET BP TO HEADER OF THE BLOCK TO BE FREED BECAUSE IT CURRENTLY POINTS TO PAYLOAD */
	sf_free_header *bp = (sf_free_header*)(((char*)ptr)-SF_HEADER_SIZE);
	bp->header.alloc = 0;
	sf_footer *foot = (sf_footer*)(((char*)bp)+(bp->header.block_size<<4) - SF_FOOTER_SIZE);
	foot->alloc = 0;
	

	#ifdef ADDRESS
		/* FREELIST WILL BE MANAGED IN INCREASING ADDRESS ORDER */
		sf_free_header *temp = freelist_head;
		if(bp < temp){
			bp->prev = NULL;
			bp->next = temp;
			temp->prev = bp;
			freelist_head = bp;

		}
		else{
			while(temp < bp && temp->next != NULL){
				temp = temp->next;
			}
			/* temp STOPPED BEFORE bp*/
			if(temp < bp){
				bp->next = temp->next;
				bp->prev = temp;
				if(temp->next != NULL){
					temp->next->prev = bp;
				}
				temp->next = bp;
			}
			/* temp STOPPED AFTER bp */
			else{
				bp->prev = temp->prev;
				temp->prev->next = bp;
				bp->next = temp;
				temp->prev = bp;
			}
		}
	#else
		/* FREELIST WILL BE MANAGED IN LAST-IN FIRST-OUT ORDER (LIFO) */
		bp->next = freelist_head;
		bp->prev = NULL;
		freelist_head->prev = bp;
		freelist_head = bp;
	
	#endif
	bp->header.requested_size = 0;
	coalesce(bp);
	sf_blockprint(bp);
}

/*
 * sf_realloc - Will take a ptr to previously allocated space and either
 *			    increase or decrease the size of the allocated space so that
 *              the size is equal to the arg "size".
 * arg ptr    - ptr to previously allocated space
 * arg size   - newly specified space of allocated space
 * returns the same ptr to the allocated space
 */
void* sf_realloc(void *ptr, size_t size) {
    
    if(size<=0)
    {
    	errno = EINVAL;
    	return NULL;
    }
    if(ptr==NULL){
    	errno = EINVAL;
    	return NULL;
    }
    if(((sf_free_header*)(((char*)ptr) - 8))->header.alloc == 0)
    	return NULL;

    sf_free_header* hd = (sf_free_header*)(((char*)ptr) - 8);
    size_t sizeOfPayload = ((hd->header.block_size)<<4)-16;


    char* payload = (char*)ptr;
    char temp[size];
    int i=0;
    for(;i<sizeOfPayload;i++){
    	temp[i]=payload[i];
    }
    sf_free(ptr);
    ptr = sf_calloc(size, 1);
    payload = (char*)ptr;
    if(ptr==NULL){
    	return NULL;
    }
    for(i=0;i<size;i++){
    	payload[i]=temp[i];
    }
    return ptr;
}

/*
 * sf_calloc - Will take an arg specifying the amount of space to allocate.
 *             Amount of space = nmemb * size
 *             Initializes newly allocated space to zero.
 * arg nmemb - the amount of elements
 * arg size  - the size of each element
 * returns a void ptr to the newly allocated space
 */
void* sf_calloc(size_t nmemb, size_t size) {
	
    size_t s = align(nmemb*size);
    void* cal = sf_malloc(nmemb*size);
    char* payl = (char*)cal;
    int i;
    for(i=0;i<s;i++){
    	payl[i]=0;
    	//printf("\nfrom the function loop: %d", payl[i]);
    }
    return cal;
}

/*
 * align        - THIS FUNCTION WILL RECIEVE A SIZE THAT MUST BE ALIGNED TO A MULTIPLE
 *                OF 16 SO THAT THE DYNAMIC ALLOCATOR CAN RUN EFFICIENTLY.
 * arg reqBytes - THE REQUESTED NUMBER OF BYTES FROM THE USER THAT MUST BE ALIGNED.
 */ 
size_t align(size_t reqBytes){

	/* 
	 * THIS WILL GUARENTEE AT LEAST 16 OR SOME MULTIPLE OF 16 WILL BE THE RETURNED ALIGNMENT
	 * ADDING THE 15 IS WHAT MAKES IT ALWAYS ROUND UP TO AT LEAST 16.
	 * THE "~" OPERATOR FLIPS THE BITS TO MAKE IT ALL 1'S ON THE LEFT OF 4 0'S.
	 */
	return ((reqBytes + 15) & ~0xF);
}

/*
 * findFit  - THIS FUNCTION FINDS THE FIRST/NEXT AVAILABLE FREE BLOCK OF AN ACCEPTABLE SIZE
 *            TO ALLOCATE MEMORY TO.
 * arg size - THE AMOUNT OF SPACE A FREE BLOCK MUST HAVE IN ORDER TO ALLOCATE THIS MEMORY.
 * returns A PTR TO THE FREE BLOCK THAT CONTAINS ENOUGH SPACE TO ALLOCATE THE CORRECT SIZE OF MEMORY.
 */
sf_free_header* findFit(size_t size){
	
	#ifdef NEXT
		/* 
		 * IF FIT-NEXT, CREATE A PREV LIST HEAD PTR TO THAT WHEN tempFreeHead LOOPS BACK 
		 * LATER, IT KNOWS WHERE IT BEGAN AND TO THEN STOP IF NOTHING IS FOUND.
		 */
		sf_free_header *prevListHead = tempFreeHead;
	#else
		/* IF NOT FIT-NEXT THEN FIT-FIRST, CHECK THE FIRST FREE BLOCK IN CASE IT'S THE ONLY FREE BLOCK */
		if((((tempFreeHead->header.block_size<<4) - SF_HEADER_SIZE - SF_FOOTER_SIZE) >= size))
			return tempFreeHead;

	#endif
	if((((tempFreeHead->header.block_size<<4) - SF_HEADER_SIZE - SF_FOOTER_SIZE) >= size))
			return tempFreeHead;

	/* RUN LOOP UNTIL END OF THE LIST SEARCHING FOR LARGE ENOUGH FREE BLOCK */	
	for(; tempFreeHead->next != NULL; tempFreeHead = tempFreeHead->next){
		if((((tempFreeHead->header.block_size<<4) - SF_HEADER_SIZE - SF_FOOTER_SIZE) >= size))
			return tempFreeHead;
	}

	#ifdef NEXT
		/* START FROM BEGINNING OF LIST AND SEARCH UP TO THE START OF THE NEXT-FIT SEARCH */
		tempFreeHead = freelist_head;
		for(; tempFreeHead->next != prevListHead; tempFreeHead = tempFreeHead->next){
			if(tempFreeHead==prevListHead)
				return NULL;
			if((((tempFreeHead->header.block_size<<4) - SF_HEADER_SIZE - SF_FOOTER_SIZE) >= size))
				return tempFreeHead;
		}
	#endif
	return NULL;
}

void updateHeaderFooter(size_t oldSize, size_t payLoadSize, sf_free_header* allocated){
	/* CHANGE FREE BLOCK BLOCK HEADER & FOOTER TO REFLECT SMALLER SIZE IF THE FREE BLK WAS SPLIT */
	//footer = (sf_footer*)(((char*)allocated) + SF_HEADER_SIZE + payLoadSize);
	tempFreeHead->header.block_size = oldSize - footer->block_size;
	tempFreeHead->header.alloc = 0;
	footer = (sf_footer*)(((char*)tempFreeHead) + (tempFreeHead->header.block_size<<4) - SF_FOOTER_SIZE);
	footer->block_size = tempFreeHead->header.block_size;
	footer->alloc = tempFreeHead->header.alloc;
}

void coalesce(sf_free_header *bp){
	int prevAlloc;
	int nextAlloc=1;
	size_t newSize = bp->header.block_size;
	

	prevAlloc = ((sf_footer*)(((char*)bp)-SF_FOOTER_SIZE))->alloc;
	if(((((char*)bp)+(newSize<<4))) != sf_sbrk(0))
	{
       	nextAlloc= ((sf_footer*)(((char*)bp) + (newSize<<4)))->alloc;
	}
	/* CASE 1 - NO COALESCING NEEDED */
	if(prevAlloc && nextAlloc)
		return;

	/* FREELIST MANAGEMENT POLICY IS SET TO ADDRESS ORDER. COALESCE ACCORDINGLY */
	#ifdef ADDRESS
		sf_free_header *p;
		sf_free_header *n;
		/* CASE 2 - BP MUST COALESCE WITH THE FREE BLK IN FRONT OF IT */
		if(prevAlloc && !nextAlloc)
		{
			/* CREATE PTR TO THE NEXT FREE BLK */
       		n = bp->next;
       		newSize += n->header.block_size;
       		bp->next = bp->next->next;
       		if(bp->next != NULL)
       			bp->next->prev = bp;
       		bp->header.block_size = newSize;
			sf_footer* newFoot =(sf_footer*)((((char*)bp)+((newSize << 4) - SF_FOOTER_SIZE)));
			newFoot->block_size = newSize;
			newSize++; //who cares just need another line for debugging

		}
		/* CASE 3 - BP MUST COALESCE WITH THE FREE BLK BEHIND IT */
		else if(!prevAlloc && nextAlloc){
			/* CREATE PTR TO THE PREVIOUS FREE BLK */
   			p = bp->prev;
			newSize += p->header.block_size;
			p->next = bp->next;
			if(bp->next != NULL)
       			p->next->prev = bp;
       		bp = p;
       		bp->header.block_size = newSize;
			sf_footer* newFoot =(sf_footer*)((((char*)bp)+((newSize << 4) - SF_FOOTER_SIZE)));
			newFoot->block_size = newSize;
			newSize++; //who cares just need another line for debugging
			

		}
		/* CASE 4 - BP MUST COALESCE WITH BOTH THE FREE BLK BEHIND IT AND IN FRONT OF IT */
		else{
			/* CREATE PTR TO THE NEXT FREE BLK */
       		n = bp->next;
       		/* CREATE PTR TO THE PREVIOUS FREE BLK */
			p = bp->prev;
			/* SUM UP THE TOTAL BLK SIZE */
			newSize += p->header.block_size + n->header.block_size;
			if(bp->next->next == NULL){
				bp->prev->next = bp->next->next;
				bp = p;
			}
			else{
				bp->prev->next = bp->next->next;
				bp->next->next->prev = bp->prev;
				bp=p;
			}
			bp->header.block_size = newSize;
			sf_footer* newFoot =(sf_footer*)((((char*)bp)+((newSize << 4) - SF_FOOTER_SIZE)));
			newFoot->block_size = newSize;
			newSize++; //who cares just need another line for debugging
		}
	#endif
}





